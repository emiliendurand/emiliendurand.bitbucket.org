/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/assets/js/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	
	/**
	 * Create a Private Scope Using an Immediately Invoked Function Expression
	 */
	(function(window, document, undefined ) {
	
		"use strict";
	
	
	    //variables
		var settings = {};
	
	    settings.authMode = "normal";
	    settings.checkChallengeTransactionTryout = 1;
		settings.urls = new Object();
		settings.urls.baseUrl = "http://91.134.141.46:80"
		settings.urls.getPartnerEndpoint = settings.urls.baseUrl + "/getPartnerEndpoint";
		settings.urls.newTransaction = settings.urls.baseUrl + "/newTransaction";
		settings.urls.checkChallengeTransaction = settings.urls.baseUrl + "/checkChallengeTransaction";
		settings.urls.waitForTrx = settings.urls.baseUrl + "/waitForTrx";
	
	    settings.urls.eshopEndpointSub = settings.urls.baseUrl + "/eshopEndpointSub";
	
	
		//components
		var izyConnectLabel,
		izyConnectBtnStart,
		izyConnectInputGsm,
		izyConnectInputCode,
		izyConnectInputSubscribe,
		izyConnectTooltip,
	    izyConnectProgressBar,
	    izyConnectStatus,
	    izyConnectApi = __webpack_require__(1);
	
		//dependencies
	    var delayed = __webpack_require__(7);
	   //var xhr = require('xhr');
	
	
	
	
		/**
		 * init btn if markup is on the page
		 * @return {[type]} [description]
		 */
		var init = function (){
			//cache wrapper
			settings.wrapperBtn = document.getElementsByClassName("izyconnect-button")[0];
	
			//check if markup is here
			if(!!settings.wrapperBtn){
	
	            // cache user agent & partnerid
	            settings.userAgent = navigator.userAgent
	            settings.fpartnerid = settings.wrapperBtn.dataset.partnerid;
	
	            //built the btn
				_buildBtn();
	
	            //
	            _getPartnerEndpoint();
	
	            // init input gsm
	            izyConnectInputGsm.init(_newTransaction);
	
	            // bind listener click btn
	            izyConnectBtnStart.bindUiActions();
			}
			else{
				throw new Error("izyConnect btn markup is not in the page")
				return false;
			}
		};
	
	
	
		/**
		 * build btn
		 * @return {[type]} [description]
		 */
		var _buildBtn = function (){
		    //console.log("building the btn");
	
			// load html
			settings.wrapperBtn.innerHTML = __webpack_require__(8);
	
			// load css
			__webpack_require__(9);
	
			// load components
	        izyConnectStatus = __webpack_require__(13),
	        izyConnectLabel = __webpack_require__(15);
	        izyConnectBtnStart = __webpack_require__(16);
	        izyConnectInputGsm = __webpack_require__(17);
	        izyConnectInputCode = __webpack_require__(20);
	        izyConnectInputSubscribe = __webpack_require__(22);
	        izyConnectTooltip = __webpack_require__(18);
	        izyConnectProgressBar = __webpack_require__(19);
		};
	
	
	
		/**
		 * destroy btn
		 * @return {[type]} [description]
		 */
		var _destroyBtn = function (){
			//settings.wrapperBtn.innerHTML = "";
	        izyConnectInputCode.destroyInput();
	        izyConnectInputGsm.destroyInput();
	        izyConnectInputSubscribe.destroyInput();
	        izyConnectBtnStart.destroyElement();
		};
	
	    /**
	     * reset btn
	     * @return {[type]} [description]
	     */
	    var _resetBtn = function (){
	
	        delayed.delay(function () {
	            izyConnectStatus.setMessage("Reinitialisation…");
	            izyConnectStatus.setStatus("loading");
	        }, 1000);
	
	        delayed.delay(function () {
	            izyConnectInputCode.resetInput();
	            izyConnectInputCode.hideInput();
	
	            izyConnectInputGsm.resetInput();
	            izyConnectInputGsm.hideInput();
	
	            izyConnectInputSubscribe.resetInput();
	            izyConnectInputSubscribe.hideInput();
	
	            izyConnectBtnStart.showElement();
	            izyConnectStatus.animateOut();
	
	            izyConnectProgressBar.hideElement();
	        }, 1500);
	
	    };
	
	
	
	
	
	    /**
	     * _getPartnerEndpoint
	     * @return {[type]} [description]
	     */
	    var _getPartnerEndpoint = function(){
	
	        var body = JSON.stringify({
	            "fpartnerid": settings.fpartnerid,
	            "ua": settings.userAgent
	        })
	
	        izyConnectApi.makeRequest(settings.urls.getPartnerEndpoint, body)
	        .then(function(resp) {
	
	            switch (resp.statusCode) {
	                case 200:
	                    try {
	                        settings.urls.endpointAuth = JSON.parse(resp.body).endpointAuth;
	                        settings.urls.endpointRegister = JSON.parse(resp.body).endpointRegister;
	                    }
	                    catch(err) {throw new Error(err);}
	
	                    break;
	
	                default:
	                    izyConnectStatus.setMessage("partnerid incorrect");
	                    izyConnectStatus.setStatus("error");
	                    izyConnectStatus.animateIn();
	                    _destroyBtn();
	                    throw new Error("can't match response code:" + resp.statusCode);
	                    break;
	            }
	        })
	        .catch(function(error) {
	            console.log(error);
	        });
	    };
	
	
	
	
	
	    /**
	     * _newTransaction
	     * @return {[type]} [description]
	     */
	    var _newTransaction = function (gsm){
	
	        //store gsm
	        settings.gsm = gsm;
	
	        var body = JSON.stringify({
	            "fpartnerid": settings.fpartnerid,
	            "gsm": settings.gsm
	        });
	
	        //validate from server
	        izyConnectApi.makeRequest(settings.urls.newTransaction, body)
	        .then(function(resp) {
	            console.log(resp);
	            switch (resp.statusCode) {
	                case 200:
	                    //store token
	                    try {
	                        var jsonBody = JSON.parse(resp.body);
	                        settings.token = jsonBody.token;
	                        settings.precode = jsonBody.precode;
	                    }
	                    catch(err) {throw new Error(err);}
	
	                    //destroy input gsm
	                    izyConnectInputGsm.hideInput();
	                    //set status message
	                    izyConnectStatus.setMessage("Compte existant");
	                    izyConnectStatus.setStatus("check");
	                    izyConnectProgressBar.setProgress("1/3");
	
	
	                    delayed.delay(function () {
	                        //hide status overlay
	                        izyConnectStatus.setMessage("Envoi du code sms");
	                        izyConnectStatus.setStatus("loading");
	                    }, 1000)
	
	                    delayed.delay(function () {
	                        //init input code
	                        izyConnectInputCode.init(_checkChallengeTransaction, settings.precode); //ajouter precode
	                        //hide status overlay
	                        izyConnectStatus.animateOut();
	                    }, 2000)
	
	
	                    //send promise for app login
	                    _waitForTrx();
	
	                    break;
	
	                case 206: //inscription shadow
	                    settings.authMode = "shadow";
	                    //store token
	                    try {
	                        var jsonBody = JSON.parse(resp.body);
	                        settings.token = jsonBody.token;
	                        settings.precode = jsonBody.precode;
	                    }
	                    catch(err) {throw new Error(err);}
	
	                    _registerShadow();
	
	                    break;
	
	                case 404: //pas de compte associé
	                    console.log("pas de compte associé");
	
	                    //store token
	                    try {
	                        var jsonBody = JSON.parse(resp.body);
	                        settings.token = jsonBody.token;
	                        settings.precode = jsonBody.precode;
	                    }
	                    catch(err) {throw new Error(err);}
	
	                    _setupAccountCreation();
	
	                    break;
	
	/*                case 401: //fpartnerid error (perimé ou disparu)
	                    //set status message
	                    izyConnectStatus.setMessage("partnerId inexistant");
	                    izyConnectStatus.setStatus("error");
	                    break;
	
	                case 400: //parametres incorrects
	                    //set status message
	                    izyConnectStatus.setMessage("parametres incorrects");
	                    izyConnectStatus.setStatus("error");
	                    break;
	
	                case 429: // trop de tentatives
	                    //set status message
	                    izyConnectStatus.setMessage("Trop de tentatives");
	                    izyConnectStatus.setStatus("error");
	                    break;
	                case 500: //erreur serveur
	                    //set status message
	                    izyConnectStatus.setMessage("Erreur serveur");
	                    izyConnectStatus.setStatus("error");
	                    break;*/
	
	                default:
	                    izyConnectStatus.setMessage("Erreur serveur");
	                    izyConnectStatus.setStatus("error");
	                    throw new Error("can't match response code on _newTransaction:" + resp.statusCode);
	                    break;
	            }
	
	        })
	        .catch(function(error) {
	            console.log(error);
	        });
	
	    };
	
	
	
	
		/**
		 * _waitForTrx
		 * @return {[type]} [description]
		 */
		var _waitForTrx = function (){
	
	        var body = JSON.stringify({
	            "gsm":settings.gsm,
	            "fpartnerid":settings.fpartnerid
	        });
	
			izyConnectApi.makeRequest(settings.urls.waitForTrx, body)
			.then(function(resp) {
				// continue
				console.log("response", resp);
	
				switch (resp.statusCode) {
	
	                case 200: //unlocked via app
	                    console.log("client unlocked via app");
	
	                    //store token
	                    try {
	                        settings.token = JSON.parse(resp.body).token;
	                    }
	                    catch(err) {console.log(err);}
	
	                    //remove tooltip
	                    izyConnectTooltip.animateOut();
	                    izyConnectLabel.animateOut();
	
	                    //update progrressbar
	                    izyConnectProgressBar.setProgress("2/3");
	
	                    //set status message identification sucess
	                    izyConnectStatus.setMessage("Identifié via l'application");
	                    izyConnectStatus.animateIn();
	                    izyConnectStatus.setStatus("check");
	
	                    //update progressbar
	                    delayed.delay(function () {
	
	                        izyConnectStatus.setMessage("Récupération des données partenaires");
	                        izyConnectStatus.animateIn();
	                        izyConnectStatus.setStatus("loading");
	
	                        //redirection
	                        _getForwardUrl();
	                    }, 2000);
	
						break;
	
	                case 408: //timeout
	                    //on relance la requette
	                    _waitForTrx();
	
	                    break;
					default:
	                    throw new Error("can't match response code on _waitForTrx:" + resp.statusCode);
				}
			})
			.catch(function(error) {
	            console.log(error);
			});
		};
	
	
	
	
	
	
	
	    /**
	     * _register
	     *  création de compte
	     */
	    var _register = function(userData){
	
	       //store userData
	       settings.userData = userData;
	
	       var body = JSON.stringify({
	            "gsm": settings.gsm,
	            "fpartnerid": settings.fpartnerid,
	            "token": settings.token,
	            "nom": settings.userData.nom,
	            "prenom":  settings.userData.prenom,
	            "email":  settings.userData.email
	        });
	
	       //make request
	       izyConnectApi.makeRequest(settings.urls.endpointRegister, body)
	       .then(function(resp) {
	            console.log("response", resp);
	
	            switch (resp.statusCode) {
	                case 200:           
	                    //remove subscribe form
	                    izyConnectInputSubscribe.hideInput();
	                    
	                    //set status message
	                    izyConnectStatus.setMessage("Compte créé");
	                    izyConnectStatus.setStatus("check");
	
	                    //update progrressbar
	                    //izyConnectProgressBar.setSteps("3");
	                    delayed.delay(function () {
	                        izyConnectStatus.setMessage("Envoi du code sms");
	                        izyConnectStatus.setStatus("loading");
	                    }, 1000)
	                    delayed.delay(function () {
	                        izyConnectStatus.setStatus("check");
	                    }, 2000)
	                    delayed.delay(function () {
	                        //init input code
	                        izyConnectInputCode.init(_checkChallengeTransaction, settings.precode); //ajouter precode
	                        //hide status overlay
	                        izyConnectStatus.animateOut();
	                    }, 3000)
	
	                    break;
	
	                case 404:
	                    if(settings.authMode === "shadow"){
	                        console.log("pas de compte associé");
	                        _setupAccountCreation();
	                    }
	                    else{
	                        settings.authMode = "shadow";
	                        _registerShadow();
	                    }
	                    break;
	
	
	                case 408: //timeout token expiré
	                    izyConnectStatus.setMessage("Token expiré");
	                    izyConnectStatus.setStatus("error");
	                    _resetBtn();
	                    throw new Error("Too Many Requests");
	                    break;
	
	                case 429: //Too Many Requests
	                    izyConnectStatus.setMessage("Trop de requettes");
	                    izyConnectStatus.setStatus("error");
	                    _resetBtn();
	                    throw new Error("Too Many Requests");
	                    break;
	
	                case 409: //votre compte existe deja avec cet email
	                    _setupAccountCreation();
	                    break;
	
	                default:
	                    throw new Error("can't match response code on _register:" + resp.statusCode);
	                    break;
	            }
	        }).catch(function(error) {
	           console.log(error);
	        });   
	    };
	
	
	
	
	
	    /**
	     * _registerShadow
	     * inscription shadow
	     */
	    var _registerShadow = function (){
	        console.log("inscription shadow");
	        var body = JSON.stringify({
	            "gsm": settings.gsm,
	            "token": settings.token
	        })
	
	
	        izyConnectApi.makeRequest(settings.urls.endpointRegister, body)
	            .then(function(resp) {
	                // continue
	                console.log("response", resp);
	
	                switch (resp.statusCode) {
	                    case 200:
	
	                        //destroy input gsm
	                        izyConnectInputGsm.hideInput();
	
	                        //set status message
	                        izyConnectStatus.setMessage("Compte existant");
	                        izyConnectStatus.setStatus("check");
	                        //update progressbar
	                        izyConnectProgressBar.setProgress("1/3");
	
	                        delayed.delay(function () {
	                            //hide status overlay
	                            izyConnectStatus.setMessage("Envoi du code sms");
	                            izyConnectStatus.setStatus("loading");
	                        }, 1000)
	                        delayed.delay(function () {
	                            //init input code
	                            izyConnectInputCode.init(_checkChallengeTransaction, settings.precode); //ajouter precode
	
	                            //hide status overlay
	                            izyConnectStatus.animateOut();
	                        }, 2000)
	
	                        //send promise for app login
	                        _waitForTrx();
	                        /*TODO*/
	                        /* regrouper ce code et celui du 200 dans une fonction */
	
	                        break;
	
	                    case 500: //unlocked via app
	                        console.log("500 reçu depuis l'inscription shadow");
	                        console.log("passage en mode création de compte");
	                        //ici inscription
	                        console.log("resp.body", resp.body);
	
	
	                        //store token
	                        /* try {
	                         var jsonBody = JSON.parse(resp.body);
	                         settings.token = jsonBody.token;
	                         settings.precode = jsonBody.precode;
	                         }
	                         catch(err) {
	                         console.log(err);
	                         }*/
	
	                        _setupAccountCreation();
	
	                        //set status message
	                        //izyConnectStatus.setMessage("Erreur serveur");
	                        //izyConnectStatus.setStatus("error");
	
	                        break;
	
	                    default:
	                        throw new Error("can't match response code on _registerShadow:" + resp.statusCode);
	                }
	            })
	            .catch(function(error) {
	                console.log(error);
	            });
	    };
	
	
	
	
	
	
	
	
	    /**
	     * _checkChallengeTransaction
	     *  validation du userkey
	     */
	    var _checkChallengeTransaction = function(userKey){
	        console.log(userKey);
	
	        //store userData
	        settings.userKey = userKey;
	
	        var body = JSON.stringify({
	            "token": settings.token,
	            "key": settings.precode + settings.userKey,
	            "fpartnerid": settings.fpartnerid
	        });
	
	        console.log(body);
	
	        izyConnectApi.makeRequest(settings.urls.checkChallengeTransaction, body)
	        .then(function(resp) {
	            console.log("response", resp);
	
	            switch (resp.statusCode) {
	                case 200:
	
	                    izyConnectStatus.setMessage("Code bon");
	                    izyConnectStatus.setStatus("check");
	                    izyConnectProgressBar.setProgress("2/3");
	                    delayed.delay(function () {
	                         izyConnectStatus.setMessage("Auth partenaire");
	                         izyConnectStatus.setStatus("loading");
	
	                        _endpointAuth();
	                    }, 2000);
	
	                    break;
	
	                case 401: // code ko, on reviens à la saisie du code
	                    console.log("key fausse");
	                    console.log("tentative n°:" + settings.checkChallengeTransactionTryout);
	
	                    if (settings.checkChallengeTransactionTryout > 2){
	                        //set status message
	                        izyConnectStatus.setMessage("Trop de tentatives avec key erronée");
	                        izyConnectStatus.setStatus("error");
	                        _destroyBtn();
	                        throw new Error("trop de tentatives");
	                    }
	                    else{
	                        settings.checkChallengeTransactionTryout++;
	
	                        //set status message
	                        izyConnectStatus.setMessage("Code non valide");
	                        izyConnectStatus.setStatus("error"); //ici
	                        izyConnectStatus.animateOut();
	
	                        //show info tooltip
	                        izyConnectTooltip.setMessage("Veuillez vérifier le code");
	                        izyConnectTooltip.setStatus("error");
	                        izyConnectTooltip.animateIn();
	                    }
	
	                    break;
	
	
	                case 408: //Timeout token expiré
	                    izyConnectStatus.setMessage("Token perimé");
	                    izyConnectStatus.setStatus("error");
	                    _resetBtn();
	                    throw new Error("timeout token expiré");
	                    break;
	
	                case 429: //Too Many Requests
	                    izyConnectStatus.setMessage("Trop de requettes");
	                    izyConnectStatus.setStatus("error");
	                    _resetBtn();
	                    throw new Error("Too Many Requests");
	                    break;
	
	                default:
	                    izyConnectStatus.setMessage("Erreur serveur");
	                    izyConnectStatus.setStatus("error");
	                    throw new Error("can't match response code on _checkChallengeTransaction:" + resp.statusCode);
	                    break;
	            }
	        }).catch(function(error) {
	            console.log(error);
	        });
	    };
	
	
	
	
	
	
	
	    /**
	     * _endpointAuth
	     * @return {[type]} [description]
	     */
	    var _endpointAuth = function(){
	        console.log("settings.urls.endpointAuth", settings.urls.endpointAuth);
	
	        var body = JSON.stringify({
	            "token": settings.token
	        });
	
	        izyConnectApi.makeRequest(settings.urls.endpointAut, body)
	        .then(function(resp) {
	            console.log("response", resp);
	
	            switch (resp.statusCode) {
	                case 200:
	                    //store forward url
	                    try {
	                        settings.fwd_adress = JSON.parse(resp.body).fwd_adress;
	                    }
	                    catch(err) {throw new Error(err);}
	
	                    izyConnectStatus.setMessage("Auth confirmée");
	                    izyConnectStatus.setStatus("check");
	                    izyConnectProgressBar.setProgress("3/3");
	
	                    delayed.delay(function () {
	                        izyConnectStatus.setMessage("Redirection");
	                        izyConnectStatus.setStatus("loading");
	                    }, 1000);
	                    delayed.delay(function () {
	                        window.location = settings.fwd_adress;
	                    }, 2000);
	
	                    break;
	
	                case 403: //votre compte est desactivé sur ce prestataire
	                    izyConnectStatus.setMessage("Compte désactivé");
	                    izyConnectStatus.setStatus("error");
	                    _resetBtn();
	                    throw new Error("Compte desactivé sur ce prestataire");
	                    break;
	
	                case 408: //timeout token expiré
	                    izyConnectStatus.setMessage("Token expiré");
	                    izyConnectStatus.setStatus("error");
	                    _resetBtn();
	                    throw new Error("timeout token expiré");
	                    break;
	
	                case 429: //Too Many Requests
	                    izyConnectStatus.setMessage("Trop de requettes");
	                    izyConnectStatus.setStatus("error");
	                    _resetBtn();
	                    throw new Error("Too Many Requests");
	                    break;
	
	                default:
	                    throw new Error("can't match response code on _getForwardUrl:" + resp.statusCode);
	                    break;
	            }
	        }).catch(function(error) {
	            console.log(error);
	        });
	    };
	
	
	
	
	
	
	
	
	
	
	
	    /**
	     * _setupAccountCreation
	     * @return {[type]} [description]
	     */
	    var _setupAccountCreation = function (){
	
	        //destroy input gsm
	        izyConnectInputGsm.hideInput();
	
	        //set message
	        izyConnectStatus.setMessage("Compte inexistant");
	
	        delayed.delay(function () {
	            izyConnectStatus.setMessage("Création de compte");
	
	            izyConnectProgressBar.setSteps("6");
	        }, 1000);
	
	        delayed.delay(function () {
	            //init form subscribe
	            izyConnectInputSubscribe.init(_register);
	
	            //hide status overlay
	            izyConnectStatus.animateOut();
	        }, 2000);
	    };
	
	
	
	
	
	
		init();
	
	
	})( window, document,0 );
	
	
	
	
	
	
	


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	
	/* IZYCONNECT API */
	
	module.exports = (function(){
	    'use strict';
	
	    //dependencies
	    //var xhr = require("xhr");
	    var Promise = __webpack_require__(2).Promise;
	
	
	
	    var makeRequest = function(url, body){
	
	        return new Promise(function(resolve, reject){
	
	            /* setup */
	            var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
	            xmlhttp.open("POST", url);
	            xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	            /* callback */
	            xmlhttp.onload = function () {
	                resolve({
	                    statusCode: this.status,
	                    body: this.response
	                });
	            };
	            /* error */
	            xmlhttp.onerror = function () {
	                reject({
	                    status: this.status,
	                    statusText: xmlhttp.statusText
	                });
	            };
	
	            xmlhttp.send(body);
	        });
	
	    };
	    
	
	    
	    /**
	     * makeRequest
	     */
	/*    var makeRequest = function(url, body){
	
	        return new Promise(function(resolve, reject){
	            xhr(
	                {
	                    body: body,
	                    method: "POST",
	                    uri: url,
	                    headers: {
	                        "Content-Type": "application/json;charset=UTF-8"
	                        }
	                },
	                handleResponse
	            );
	
	            function handleResponse(err, resp, body) {
	                if (err) {  reject(err);  }
	                else { resolve(resp); }
	            };
	        });
	
	    };*/
	
	
	
	
	
	
	
	
	
	
	
	    /*        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
	     xmlhttp.open("POST", "http://91.134.141.46/getPartnerEndpoint");
	     xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	     xmlhttp.onload = function () {
	     console.log(this.status);
	     if (this.status >= 200 && this.status < 300) {
	     console.log(this.response);
	     } else {
	     console.log({
	     status: this.status,
	     statusText: this.statusText
	     });
	     }
	     };
	     xmlhttp.onerror = function () {
	
	     console.log({
	     status: this.status,
	     statusText: this.statusText
	     });
	     throw new Error("error");
	     };
	     xmlhttp.send(body);*/
	
	
	
	
	
	
	
	
	
	
	    /**
	     * validateGsm
	     */
	/*    var validateGsm = function(settings){
	        var promise = new RSVP.Promise(function(resolve, reject){
	
	            xhr(
	                {
	                    body: JSON.stringify({
	                        "gsm":settings.gsm,
	                        "fpartnerid":settings.fpartnerid
	                    }),
	                    method: "POST",
	                    uri: apiUrl + "/newTransaction",
	                    headers: {
	                        "Content-Type": "application/json",
	                        "Access-Control-Allow-Origin": "*"
	                    }
	                },
	                handleResponse
	            );
	
	            function handleResponse(err, resp, body) {
	                if (err) {  reject(err);  }
	                else { resolve(resp); }
	            };
	        });
	        return promise;
	    };*/
	
	
	
	    /**
	     * waitForAppLogin
	     */
	/*
	    var waitForAppLogin = function(settings){
	
	        return new Promise(function(resolve, reject){
	
	            xhr(
	                {
	                    body: JSON.stringify({
	                        "gsm":settings.gsm,
	                        "fpartnerid":settings.fpartnerid
	                    }),
	                    method: "POST",
	                    uri: apiUrl + "/waitForTrx",
	                    headers: {
	                        "Content-Type": "application/json",
	                        "Access-Control-Allow-Origin": "*"
	                    }
	                },
	                handleResponse
	            );
	            function handleResponse(err, resp, body) {
	                if (err) {  reject(err);  }
	                else { resolve(resp); }
	            };
	
	        });
	
	    };
	*/
	
	
	
	
	    /**
	     * shadowLogin
	     */
	/*
	    var shadowLogin = function(settings){
	
	           console.log("settings.gsm", settings.gsm);
	           console.log("settings.token", settings.token);
	
	        var promise = new Promise(function(resolve, reject){
	
	            xhr(
	                {
	                    body: JSON.stringify({
	                        "gsm": settings.gsm,
	                        "token": settings.token
	                    }),
	                    method: "POST",
	                    uri: apiUrl + "/eshopEndpointSub",
	                    headers: {
	                        "Content-Type": "application/json",
	                        "Access-Control-Allow-Origin": "*"
	                    }
	                },
	                handleResponse
	            );
	            function handleResponse(err, resp, body) {
	                if (err) {  reject(err);  }
	                else { resolve(resp); }
	
	            };
	        });
	        return promise;
	    };
	*/
	
	
	
	
	    /**
	     * createAccount
	     */
	/*
	    var createAccount = function(settings){
	
	        var promise = new Promise(function(resolve, reject){
	
	            xhr(
	                {
	                    body: JSON.stringify({
	                        "gsm": settings.gsm,
	                        "fpartnerid": settings.fpartnerid,
	                        "token": settings.token,
	                        "nom": settings.userData.nom,
	                        "prenom":  settings.userData.prenom,
	                        "email":  settings.userData.email
	                    }),
	                    method: "POST",
	                    uri: apiUrl + "/eshopEndpointSub",
	                    headers: {
	                        "Content-Type": "application/json",
	                        "Access-Control-Allow-Origin": "*"
	                    }
	                },
	                handleResponse
	            );
	            function handleResponse(err, resp, body) {
	                if (err) {  reject(err);  }
	                else { resolve(resp); }
	
	            };
	        });
	        return promise;
	    };
	*/
	
	
	
	
	
	    /**
	     * validateKey
	     */
	/*    var validateKey = function(token, key){
	        console.log("token", token);
	        console.log("key", key);
	
	
	        var promise = new Promise(function(resolve, reject){
	
	            xhr(
	                {
	                    body: JSON.stringify({
	                        "token": token,
	                        "key": key
	                    }),
	                    method: "POST",
	                    uri: apiUrl + "/checkChallengeTransaction",
	                    headers: {
	                        "Content-Type": "application/json",
	                        "Access-Control-Allow-Origin": "*"
	                    }
	                },
	                handleResponse
	            );
	            function handleResponse(err, resp, body) {
	                if (err) {  reject(err);  }
	                else { resolve(resp); }
	
	            };
	        });
	        return promise;
	    };*/
	
	
	
	    /**
	     * getForwardUrl
	     */
	/*
	    var getForwardUrl = function(settings){
	        var promise = new Promise(function(resolve, reject){
	
	            xhr(
	                {
	                    body: JSON.stringify({
	                        "gsm": settings.gsm,
	                        "token": settings.token,
	                        "key": settings.key
	                    }),
	                    method: "POST",
	                    uri: apiUrl + "/eshopEndpointAuth",
	                    headers: {
	                        "Content-Type": "application/json",
	                        "Access-Control-Allow-Origin": "*"
	                    }
	                },
	                handleResponse
	            );
	            function handleResponse(err, resp, body) {
	                if (err) {  reject(err);  }
	                else { resolve(resp); }
	
	            };
	        });
	        return promise;
	    };
	*/
	
	
	
	
	
	
	
	
	
	
	
	  return {
	    //validatePartnerId: validatePartnerId,
	    //validateGsm: validateGsm,
	/*    validateKey: validateKey,
	    waitForAppLogin: waitForAppLogin,
	    createAccount: createAccount,
	    getForwardUrl: getForwardUrl,
	    shadowLogin: shadowLogin,*/
	    makeRequest:makeRequest
	  };
	
	})();

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var require;var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(process, global, module) {/*!
	 * @overview es6-promise - a tiny implementation of Promises/A+.
	 * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
	 * @license   Licensed under MIT license
	 *            See https://raw.githubusercontent.com/jakearchibald/es6-promise/master/LICENSE
	 * @version   3.2.1
	 */
	
	(function() {
	    "use strict";
	    function lib$es6$promise$utils$$objectOrFunction(x) {
	      return typeof x === 'function' || (typeof x === 'object' && x !== null);
	    }
	
	    function lib$es6$promise$utils$$isFunction(x) {
	      return typeof x === 'function';
	    }
	
	    function lib$es6$promise$utils$$isMaybeThenable(x) {
	      return typeof x === 'object' && x !== null;
	    }
	
	    var lib$es6$promise$utils$$_isArray;
	    if (!Array.isArray) {
	      lib$es6$promise$utils$$_isArray = function (x) {
	        return Object.prototype.toString.call(x) === '[object Array]';
	      };
	    } else {
	      lib$es6$promise$utils$$_isArray = Array.isArray;
	    }
	
	    var lib$es6$promise$utils$$isArray = lib$es6$promise$utils$$_isArray;
	    var lib$es6$promise$asap$$len = 0;
	    var lib$es6$promise$asap$$vertxNext;
	    var lib$es6$promise$asap$$customSchedulerFn;
	
	    var lib$es6$promise$asap$$asap = function asap(callback, arg) {
	      lib$es6$promise$asap$$queue[lib$es6$promise$asap$$len] = callback;
	      lib$es6$promise$asap$$queue[lib$es6$promise$asap$$len + 1] = arg;
	      lib$es6$promise$asap$$len += 2;
	      if (lib$es6$promise$asap$$len === 2) {
	        // If len is 2, that means that we need to schedule an async flush.
	        // If additional callbacks are queued before the queue is flushed, they
	        // will be processed by this flush that we are scheduling.
	        if (lib$es6$promise$asap$$customSchedulerFn) {
	          lib$es6$promise$asap$$customSchedulerFn(lib$es6$promise$asap$$flush);
	        } else {
	          lib$es6$promise$asap$$scheduleFlush();
	        }
	      }
	    }
	
	    function lib$es6$promise$asap$$setScheduler(scheduleFn) {
	      lib$es6$promise$asap$$customSchedulerFn = scheduleFn;
	    }
	
	    function lib$es6$promise$asap$$setAsap(asapFn) {
	      lib$es6$promise$asap$$asap = asapFn;
	    }
	
	    var lib$es6$promise$asap$$browserWindow = (typeof window !== 'undefined') ? window : undefined;
	    var lib$es6$promise$asap$$browserGlobal = lib$es6$promise$asap$$browserWindow || {};
	    var lib$es6$promise$asap$$BrowserMutationObserver = lib$es6$promise$asap$$browserGlobal.MutationObserver || lib$es6$promise$asap$$browserGlobal.WebKitMutationObserver;
	    var lib$es6$promise$asap$$isNode = typeof self === 'undefined' && typeof process !== 'undefined' && {}.toString.call(process) === '[object process]';
	
	    // test for web worker but not in IE10
	    var lib$es6$promise$asap$$isWorker = typeof Uint8ClampedArray !== 'undefined' &&
	      typeof importScripts !== 'undefined' &&
	      typeof MessageChannel !== 'undefined';
	
	    // node
	    function lib$es6$promise$asap$$useNextTick() {
	      // node version 0.10.x displays a deprecation warning when nextTick is used recursively
	      // see https://github.com/cujojs/when/issues/410 for details
	      return function() {
	        process.nextTick(lib$es6$promise$asap$$flush);
	      };
	    }
	
	    // vertx
	    function lib$es6$promise$asap$$useVertxTimer() {
	      return function() {
	        lib$es6$promise$asap$$vertxNext(lib$es6$promise$asap$$flush);
	      };
	    }
	
	    function lib$es6$promise$asap$$useMutationObserver() {
	      var iterations = 0;
	      var observer = new lib$es6$promise$asap$$BrowserMutationObserver(lib$es6$promise$asap$$flush);
	      var node = document.createTextNode('');
	      observer.observe(node, { characterData: true });
	
	      return function() {
	        node.data = (iterations = ++iterations % 2);
	      };
	    }
	
	    // web worker
	    function lib$es6$promise$asap$$useMessageChannel() {
	      var channel = new MessageChannel();
	      channel.port1.onmessage = lib$es6$promise$asap$$flush;
	      return function () {
	        channel.port2.postMessage(0);
	      };
	    }
	
	    function lib$es6$promise$asap$$useSetTimeout() {
	      return function() {
	        setTimeout(lib$es6$promise$asap$$flush, 1);
	      };
	    }
	
	    var lib$es6$promise$asap$$queue = new Array(1000);
	    function lib$es6$promise$asap$$flush() {
	      for (var i = 0; i < lib$es6$promise$asap$$len; i+=2) {
	        var callback = lib$es6$promise$asap$$queue[i];
	        var arg = lib$es6$promise$asap$$queue[i+1];
	
	        callback(arg);
	
	        lib$es6$promise$asap$$queue[i] = undefined;
	        lib$es6$promise$asap$$queue[i+1] = undefined;
	      }
	
	      lib$es6$promise$asap$$len = 0;
	    }
	
	    function lib$es6$promise$asap$$attemptVertx() {
	      try {
	        var r = require;
	        var vertx = __webpack_require__(5);
	        lib$es6$promise$asap$$vertxNext = vertx.runOnLoop || vertx.runOnContext;
	        return lib$es6$promise$asap$$useVertxTimer();
	      } catch(e) {
	        return lib$es6$promise$asap$$useSetTimeout();
	      }
	    }
	
	    var lib$es6$promise$asap$$scheduleFlush;
	    // Decide what async method to use to triggering processing of queued callbacks:
	    if (lib$es6$promise$asap$$isNode) {
	      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useNextTick();
	    } else if (lib$es6$promise$asap$$BrowserMutationObserver) {
	      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useMutationObserver();
	    } else if (lib$es6$promise$asap$$isWorker) {
	      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useMessageChannel();
	    } else if (lib$es6$promise$asap$$browserWindow === undefined && "function" === 'function') {
	      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$attemptVertx();
	    } else {
	      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useSetTimeout();
	    }
	    function lib$es6$promise$then$$then(onFulfillment, onRejection) {
	      var parent = this;
	
	      var child = new this.constructor(lib$es6$promise$$internal$$noop);
	
	      if (child[lib$es6$promise$$internal$$PROMISE_ID] === undefined) {
	        lib$es6$promise$$internal$$makePromise(child);
	      }
	
	      var state = parent._state;
	
	      if (state) {
	        var callback = arguments[state - 1];
	        lib$es6$promise$asap$$asap(function(){
	          lib$es6$promise$$internal$$invokeCallback(state, child, callback, parent._result);
	        });
	      } else {
	        lib$es6$promise$$internal$$subscribe(parent, child, onFulfillment, onRejection);
	      }
	
	      return child;
	    }
	    var lib$es6$promise$then$$default = lib$es6$promise$then$$then;
	    function lib$es6$promise$promise$resolve$$resolve(object) {
	      /*jshint validthis:true */
	      var Constructor = this;
	
	      if (object && typeof object === 'object' && object.constructor === Constructor) {
	        return object;
	      }
	
	      var promise = new Constructor(lib$es6$promise$$internal$$noop);
	      lib$es6$promise$$internal$$resolve(promise, object);
	      return promise;
	    }
	    var lib$es6$promise$promise$resolve$$default = lib$es6$promise$promise$resolve$$resolve;
	    var lib$es6$promise$$internal$$PROMISE_ID = Math.random().toString(36).substring(16);
	
	    function lib$es6$promise$$internal$$noop() {}
	
	    var lib$es6$promise$$internal$$PENDING   = void 0;
	    var lib$es6$promise$$internal$$FULFILLED = 1;
	    var lib$es6$promise$$internal$$REJECTED  = 2;
	
	    var lib$es6$promise$$internal$$GET_THEN_ERROR = new lib$es6$promise$$internal$$ErrorObject();
	
	    function lib$es6$promise$$internal$$selfFulfillment() {
	      return new TypeError("You cannot resolve a promise with itself");
	    }
	
	    function lib$es6$promise$$internal$$cannotReturnOwn() {
	      return new TypeError('A promises callback cannot return that same promise.');
	    }
	
	    function lib$es6$promise$$internal$$getThen(promise) {
	      try {
	        return promise.then;
	      } catch(error) {
	        lib$es6$promise$$internal$$GET_THEN_ERROR.error = error;
	        return lib$es6$promise$$internal$$GET_THEN_ERROR;
	      }
	    }
	
	    function lib$es6$promise$$internal$$tryThen(then, value, fulfillmentHandler, rejectionHandler) {
	      try {
	        then.call(value, fulfillmentHandler, rejectionHandler);
	      } catch(e) {
	        return e;
	      }
	    }
	
	    function lib$es6$promise$$internal$$handleForeignThenable(promise, thenable, then) {
	       lib$es6$promise$asap$$asap(function(promise) {
	        var sealed = false;
	        var error = lib$es6$promise$$internal$$tryThen(then, thenable, function(value) {
	          if (sealed) { return; }
	          sealed = true;
	          if (thenable !== value) {
	            lib$es6$promise$$internal$$resolve(promise, value);
	          } else {
	            lib$es6$promise$$internal$$fulfill(promise, value);
	          }
	        }, function(reason) {
	          if (sealed) { return; }
	          sealed = true;
	
	          lib$es6$promise$$internal$$reject(promise, reason);
	        }, 'Settle: ' + (promise._label || ' unknown promise'));
	
	        if (!sealed && error) {
	          sealed = true;
	          lib$es6$promise$$internal$$reject(promise, error);
	        }
	      }, promise);
	    }
	
	    function lib$es6$promise$$internal$$handleOwnThenable(promise, thenable) {
	      if (thenable._state === lib$es6$promise$$internal$$FULFILLED) {
	        lib$es6$promise$$internal$$fulfill(promise, thenable._result);
	      } else if (thenable._state === lib$es6$promise$$internal$$REJECTED) {
	        lib$es6$promise$$internal$$reject(promise, thenable._result);
	      } else {
	        lib$es6$promise$$internal$$subscribe(thenable, undefined, function(value) {
	          lib$es6$promise$$internal$$resolve(promise, value);
	        }, function(reason) {
	          lib$es6$promise$$internal$$reject(promise, reason);
	        });
	      }
	    }
	
	    function lib$es6$promise$$internal$$handleMaybeThenable(promise, maybeThenable, then) {
	      if (maybeThenable.constructor === promise.constructor &&
	          then === lib$es6$promise$then$$default &&
	          constructor.resolve === lib$es6$promise$promise$resolve$$default) {
	        lib$es6$promise$$internal$$handleOwnThenable(promise, maybeThenable);
	      } else {
	        if (then === lib$es6$promise$$internal$$GET_THEN_ERROR) {
	          lib$es6$promise$$internal$$reject(promise, lib$es6$promise$$internal$$GET_THEN_ERROR.error);
	        } else if (then === undefined) {
	          lib$es6$promise$$internal$$fulfill(promise, maybeThenable);
	        } else if (lib$es6$promise$utils$$isFunction(then)) {
	          lib$es6$promise$$internal$$handleForeignThenable(promise, maybeThenable, then);
	        } else {
	          lib$es6$promise$$internal$$fulfill(promise, maybeThenable);
	        }
	      }
	    }
	
	    function lib$es6$promise$$internal$$resolve(promise, value) {
	      if (promise === value) {
	        lib$es6$promise$$internal$$reject(promise, lib$es6$promise$$internal$$selfFulfillment());
	      } else if (lib$es6$promise$utils$$objectOrFunction(value)) {
	        lib$es6$promise$$internal$$handleMaybeThenable(promise, value, lib$es6$promise$$internal$$getThen(value));
	      } else {
	        lib$es6$promise$$internal$$fulfill(promise, value);
	      }
	    }
	
	    function lib$es6$promise$$internal$$publishRejection(promise) {
	      if (promise._onerror) {
	        promise._onerror(promise._result);
	      }
	
	      lib$es6$promise$$internal$$publish(promise);
	    }
	
	    function lib$es6$promise$$internal$$fulfill(promise, value) {
	      if (promise._state !== lib$es6$promise$$internal$$PENDING) { return; }
	
	      promise._result = value;
	      promise._state = lib$es6$promise$$internal$$FULFILLED;
	
	      if (promise._subscribers.length !== 0) {
	        lib$es6$promise$asap$$asap(lib$es6$promise$$internal$$publish, promise);
	      }
	    }
	
	    function lib$es6$promise$$internal$$reject(promise, reason) {
	      if (promise._state !== lib$es6$promise$$internal$$PENDING) { return; }
	      promise._state = lib$es6$promise$$internal$$REJECTED;
	      promise._result = reason;
	
	      lib$es6$promise$asap$$asap(lib$es6$promise$$internal$$publishRejection, promise);
	    }
	
	    function lib$es6$promise$$internal$$subscribe(parent, child, onFulfillment, onRejection) {
	      var subscribers = parent._subscribers;
	      var length = subscribers.length;
	
	      parent._onerror = null;
	
	      subscribers[length] = child;
	      subscribers[length + lib$es6$promise$$internal$$FULFILLED] = onFulfillment;
	      subscribers[length + lib$es6$promise$$internal$$REJECTED]  = onRejection;
	
	      if (length === 0 && parent._state) {
	        lib$es6$promise$asap$$asap(lib$es6$promise$$internal$$publish, parent);
	      }
	    }
	
	    function lib$es6$promise$$internal$$publish(promise) {
	      var subscribers = promise._subscribers;
	      var settled = promise._state;
	
	      if (subscribers.length === 0) { return; }
	
	      var child, callback, detail = promise._result;
	
	      for (var i = 0; i < subscribers.length; i += 3) {
	        child = subscribers[i];
	        callback = subscribers[i + settled];
	
	        if (child) {
	          lib$es6$promise$$internal$$invokeCallback(settled, child, callback, detail);
	        } else {
	          callback(detail);
	        }
	      }
	
	      promise._subscribers.length = 0;
	    }
	
	    function lib$es6$promise$$internal$$ErrorObject() {
	      this.error = null;
	    }
	
	    var lib$es6$promise$$internal$$TRY_CATCH_ERROR = new lib$es6$promise$$internal$$ErrorObject();
	
	    function lib$es6$promise$$internal$$tryCatch(callback, detail) {
	      try {
	        return callback(detail);
	      } catch(e) {
	        lib$es6$promise$$internal$$TRY_CATCH_ERROR.error = e;
	        return lib$es6$promise$$internal$$TRY_CATCH_ERROR;
	      }
	    }
	
	    function lib$es6$promise$$internal$$invokeCallback(settled, promise, callback, detail) {
	      var hasCallback = lib$es6$promise$utils$$isFunction(callback),
	          value, error, succeeded, failed;
	
	      if (hasCallback) {
	        value = lib$es6$promise$$internal$$tryCatch(callback, detail);
	
	        if (value === lib$es6$promise$$internal$$TRY_CATCH_ERROR) {
	          failed = true;
	          error = value.error;
	          value = null;
	        } else {
	          succeeded = true;
	        }
	
	        if (promise === value) {
	          lib$es6$promise$$internal$$reject(promise, lib$es6$promise$$internal$$cannotReturnOwn());
	          return;
	        }
	
	      } else {
	        value = detail;
	        succeeded = true;
	      }
	
	      if (promise._state !== lib$es6$promise$$internal$$PENDING) {
	        // noop
	      } else if (hasCallback && succeeded) {
	        lib$es6$promise$$internal$$resolve(promise, value);
	      } else if (failed) {
	        lib$es6$promise$$internal$$reject(promise, error);
	      } else if (settled === lib$es6$promise$$internal$$FULFILLED) {
	        lib$es6$promise$$internal$$fulfill(promise, value);
	      } else if (settled === lib$es6$promise$$internal$$REJECTED) {
	        lib$es6$promise$$internal$$reject(promise, value);
	      }
	    }
	
	    function lib$es6$promise$$internal$$initializePromise(promise, resolver) {
	      try {
	        resolver(function resolvePromise(value){
	          lib$es6$promise$$internal$$resolve(promise, value);
	        }, function rejectPromise(reason) {
	          lib$es6$promise$$internal$$reject(promise, reason);
	        });
	      } catch(e) {
	        lib$es6$promise$$internal$$reject(promise, e);
	      }
	    }
	
	    var lib$es6$promise$$internal$$id = 0;
	    function lib$es6$promise$$internal$$nextId() {
	      return lib$es6$promise$$internal$$id++;
	    }
	
	    function lib$es6$promise$$internal$$makePromise(promise) {
	      promise[lib$es6$promise$$internal$$PROMISE_ID] = lib$es6$promise$$internal$$id++;
	      promise._state = undefined;
	      promise._result = undefined;
	      promise._subscribers = [];
	    }
	
	    function lib$es6$promise$promise$all$$all(entries) {
	      return new lib$es6$promise$enumerator$$default(this, entries).promise;
	    }
	    var lib$es6$promise$promise$all$$default = lib$es6$promise$promise$all$$all;
	    function lib$es6$promise$promise$race$$race(entries) {
	      /*jshint validthis:true */
	      var Constructor = this;
	
	      if (!lib$es6$promise$utils$$isArray(entries)) {
	        return new Constructor(function(resolve, reject) {
	          reject(new TypeError('You must pass an array to race.'));
	        });
	      } else {
	        return new Constructor(function(resolve, reject) {
	          var length = entries.length;
	          for (var i = 0; i < length; i++) {
	            Constructor.resolve(entries[i]).then(resolve, reject);
	          }
	        });
	      }
	    }
	    var lib$es6$promise$promise$race$$default = lib$es6$promise$promise$race$$race;
	    function lib$es6$promise$promise$reject$$reject(reason) {
	      /*jshint validthis:true */
	      var Constructor = this;
	      var promise = new Constructor(lib$es6$promise$$internal$$noop);
	      lib$es6$promise$$internal$$reject(promise, reason);
	      return promise;
	    }
	    var lib$es6$promise$promise$reject$$default = lib$es6$promise$promise$reject$$reject;
	
	
	    function lib$es6$promise$promise$$needsResolver() {
	      throw new TypeError('You must pass a resolver function as the first argument to the promise constructor');
	    }
	
	    function lib$es6$promise$promise$$needsNew() {
	      throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
	    }
	
	    var lib$es6$promise$promise$$default = lib$es6$promise$promise$$Promise;
	    /**
	      Promise objects represent the eventual result of an asynchronous operation. The
	      primary way of interacting with a promise is through its `then` method, which
	      registers callbacks to receive either a promise's eventual value or the reason
	      why the promise cannot be fulfilled.
	
	      Terminology
	      -----------
	
	      - `promise` is an object or function with a `then` method whose behavior conforms to this specification.
	      - `thenable` is an object or function that defines a `then` method.
	      - `value` is any legal JavaScript value (including undefined, a thenable, or a promise).
	      - `exception` is a value that is thrown using the throw statement.
	      - `reason` is a value that indicates why a promise was rejected.
	      - `settled` the final resting state of a promise, fulfilled or rejected.
	
	      A promise can be in one of three states: pending, fulfilled, or rejected.
	
	      Promises that are fulfilled have a fulfillment value and are in the fulfilled
	      state.  Promises that are rejected have a rejection reason and are in the
	      rejected state.  A fulfillment value is never a thenable.
	
	      Promises can also be said to *resolve* a value.  If this value is also a
	      promise, then the original promise's settled state will match the value's
	      settled state.  So a promise that *resolves* a promise that rejects will
	      itself reject, and a promise that *resolves* a promise that fulfills will
	      itself fulfill.
	
	
	      Basic Usage:
	      ------------
	
	      ```js
	      var promise = new Promise(function(resolve, reject) {
	        // on success
	        resolve(value);
	
	        // on failure
	        reject(reason);
	      });
	
	      promise.then(function(value) {
	        // on fulfillment
	      }, function(reason) {
	        // on rejection
	      });
	      ```
	
	      Advanced Usage:
	      ---------------
	
	      Promises shine when abstracting away asynchronous interactions such as
	      `XMLHttpRequest`s.
	
	      ```js
	      function getJSON(url) {
	        return new Promise(function(resolve, reject){
	          var xhr = new XMLHttpRequest();
	
	          xhr.open('GET', url);
	          xhr.onreadystatechange = handler;
	          xhr.responseType = 'json';
	          xhr.setRequestHeader('Accept', 'application/json');
	          xhr.send();
	
	          function handler() {
	            if (this.readyState === this.DONE) {
	              if (this.status === 200) {
	                resolve(this.response);
	              } else {
	                reject(new Error('getJSON: `' + url + '` failed with status: [' + this.status + ']'));
	              }
	            }
	          };
	        });
	      }
	
	      getJSON('/posts.json').then(function(json) {
	        // on fulfillment
	      }, function(reason) {
	        // on rejection
	      });
	      ```
	
	      Unlike callbacks, promises are great composable primitives.
	
	      ```js
	      Promise.all([
	        getJSON('/posts'),
	        getJSON('/comments')
	      ]).then(function(values){
	        values[0] // => postsJSON
	        values[1] // => commentsJSON
	
	        return values;
	      });
	      ```
	
	      @class Promise
	      @param {function} resolver
	      Useful for tooling.
	      @constructor
	    */
	    function lib$es6$promise$promise$$Promise(resolver) {
	      this[lib$es6$promise$$internal$$PROMISE_ID] = lib$es6$promise$$internal$$nextId();
	      this._result = this._state = undefined;
	      this._subscribers = [];
	
	      if (lib$es6$promise$$internal$$noop !== resolver) {
	        typeof resolver !== 'function' && lib$es6$promise$promise$$needsResolver();
	        this instanceof lib$es6$promise$promise$$Promise ? lib$es6$promise$$internal$$initializePromise(this, resolver) : lib$es6$promise$promise$$needsNew();
	      }
	    }
	
	    lib$es6$promise$promise$$Promise.all = lib$es6$promise$promise$all$$default;
	    lib$es6$promise$promise$$Promise.race = lib$es6$promise$promise$race$$default;
	    lib$es6$promise$promise$$Promise.resolve = lib$es6$promise$promise$resolve$$default;
	    lib$es6$promise$promise$$Promise.reject = lib$es6$promise$promise$reject$$default;
	    lib$es6$promise$promise$$Promise._setScheduler = lib$es6$promise$asap$$setScheduler;
	    lib$es6$promise$promise$$Promise._setAsap = lib$es6$promise$asap$$setAsap;
	    lib$es6$promise$promise$$Promise._asap = lib$es6$promise$asap$$asap;
	
	    lib$es6$promise$promise$$Promise.prototype = {
	      constructor: lib$es6$promise$promise$$Promise,
	
	    /**
	      The primary way of interacting with a promise is through its `then` method,
	      which registers callbacks to receive either a promise's eventual value or the
	      reason why the promise cannot be fulfilled.
	
	      ```js
	      findUser().then(function(user){
	        // user is available
	      }, function(reason){
	        // user is unavailable, and you are given the reason why
	      });
	      ```
	
	      Chaining
	      --------
	
	      The return value of `then` is itself a promise.  This second, 'downstream'
	      promise is resolved with the return value of the first promise's fulfillment
	      or rejection handler, or rejected if the handler throws an exception.
	
	      ```js
	      findUser().then(function (user) {
	        return user.name;
	      }, function (reason) {
	        return 'default name';
	      }).then(function (userName) {
	        // If `findUser` fulfilled, `userName` will be the user's name, otherwise it
	        // will be `'default name'`
	      });
	
	      findUser().then(function (user) {
	        throw new Error('Found user, but still unhappy');
	      }, function (reason) {
	        throw new Error('`findUser` rejected and we're unhappy');
	      }).then(function (value) {
	        // never reached
	      }, function (reason) {
	        // if `findUser` fulfilled, `reason` will be 'Found user, but still unhappy'.
	        // If `findUser` rejected, `reason` will be '`findUser` rejected and we're unhappy'.
	      });
	      ```
	      If the downstream promise does not specify a rejection handler, rejection reasons will be propagated further downstream.
	
	      ```js
	      findUser().then(function (user) {
	        throw new PedagogicalException('Upstream error');
	      }).then(function (value) {
	        // never reached
	      }).then(function (value) {
	        // never reached
	      }, function (reason) {
	        // The `PedgagocialException` is propagated all the way down to here
	      });
	      ```
	
	      Assimilation
	      ------------
	
	      Sometimes the value you want to propagate to a downstream promise can only be
	      retrieved asynchronously. This can be achieved by returning a promise in the
	      fulfillment or rejection handler. The downstream promise will then be pending
	      until the returned promise is settled. This is called *assimilation*.
	
	      ```js
	      findUser().then(function (user) {
	        return findCommentsByAuthor(user);
	      }).then(function (comments) {
	        // The user's comments are now available
	      });
	      ```
	
	      If the assimliated promise rejects, then the downstream promise will also reject.
	
	      ```js
	      findUser().then(function (user) {
	        return findCommentsByAuthor(user);
	      }).then(function (comments) {
	        // If `findCommentsByAuthor` fulfills, we'll have the value here
	      }, function (reason) {
	        // If `findCommentsByAuthor` rejects, we'll have the reason here
	      });
	      ```
	
	      Simple Example
	      --------------
	
	      Synchronous Example
	
	      ```javascript
	      var result;
	
	      try {
	        result = findResult();
	        // success
	      } catch(reason) {
	        // failure
	      }
	      ```
	
	      Errback Example
	
	      ```js
	      findResult(function(result, err){
	        if (err) {
	          // failure
	        } else {
	          // success
	        }
	      });
	      ```
	
	      Promise Example;
	
	      ```javascript
	      findResult().then(function(result){
	        // success
	      }, function(reason){
	        // failure
	      });
	      ```
	
	      Advanced Example
	      --------------
	
	      Synchronous Example
	
	      ```javascript
	      var author, books;
	
	      try {
	        author = findAuthor();
	        books  = findBooksByAuthor(author);
	        // success
	      } catch(reason) {
	        // failure
	      }
	      ```
	
	      Errback Example
	
	      ```js
	
	      function foundBooks(books) {
	
	      }
	
	      function failure(reason) {
	
	      }
	
	      findAuthor(function(author, err){
	        if (err) {
	          failure(err);
	          // failure
	        } else {
	          try {
	            findBoooksByAuthor(author, function(books, err) {
	              if (err) {
	                failure(err);
	              } else {
	                try {
	                  foundBooks(books);
	                } catch(reason) {
	                  failure(reason);
	                }
	              }
	            });
	          } catch(error) {
	            failure(err);
	          }
	          // success
	        }
	      });
	      ```
	
	      Promise Example;
	
	      ```javascript
	      findAuthor().
	        then(findBooksByAuthor).
	        then(function(books){
	          // found books
	      }).catch(function(reason){
	        // something went wrong
	      });
	      ```
	
	      @method then
	      @param {Function} onFulfilled
	      @param {Function} onRejected
	      Useful for tooling.
	      @return {Promise}
	    */
	      then: lib$es6$promise$then$$default,
	
	    /**
	      `catch` is simply sugar for `then(undefined, onRejection)` which makes it the same
	      as the catch block of a try/catch statement.
	
	      ```js
	      function findAuthor(){
	        throw new Error('couldn't find that author');
	      }
	
	      // synchronous
	      try {
	        findAuthor();
	      } catch(reason) {
	        // something went wrong
	      }
	
	      // async with promises
	      findAuthor().catch(function(reason){
	        // something went wrong
	      });
	      ```
	
	      @method catch
	      @param {Function} onRejection
	      Useful for tooling.
	      @return {Promise}
	    */
	      'catch': function(onRejection) {
	        return this.then(null, onRejection);
	      }
	    };
	    var lib$es6$promise$enumerator$$default = lib$es6$promise$enumerator$$Enumerator;
	    function lib$es6$promise$enumerator$$Enumerator(Constructor, input) {
	      this._instanceConstructor = Constructor;
	      this.promise = new Constructor(lib$es6$promise$$internal$$noop);
	
	      if (!this.promise[lib$es6$promise$$internal$$PROMISE_ID]) {
	        lib$es6$promise$$internal$$makePromise(this.promise);
	      }
	
	      if (lib$es6$promise$utils$$isArray(input)) {
	        this._input     = input;
	        this.length     = input.length;
	        this._remaining = input.length;
	
	        this._result = new Array(this.length);
	
	        if (this.length === 0) {
	          lib$es6$promise$$internal$$fulfill(this.promise, this._result);
	        } else {
	          this.length = this.length || 0;
	          this._enumerate();
	          if (this._remaining === 0) {
	            lib$es6$promise$$internal$$fulfill(this.promise, this._result);
	          }
	        }
	      } else {
	        lib$es6$promise$$internal$$reject(this.promise, lib$es6$promise$enumerator$$validationError());
	      }
	    }
	
	    function lib$es6$promise$enumerator$$validationError() {
	      return new Error('Array Methods must be provided an Array');
	    }
	
	    lib$es6$promise$enumerator$$Enumerator.prototype._enumerate = function() {
	      var length  = this.length;
	      var input   = this._input;
	
	      for (var i = 0; this._state === lib$es6$promise$$internal$$PENDING && i < length; i++) {
	        this._eachEntry(input[i], i);
	      }
	    };
	
	    lib$es6$promise$enumerator$$Enumerator.prototype._eachEntry = function(entry, i) {
	      var c = this._instanceConstructor;
	      var resolve = c.resolve;
	
	      if (resolve === lib$es6$promise$promise$resolve$$default) {
	        var then = lib$es6$promise$$internal$$getThen(entry);
	
	        if (then === lib$es6$promise$then$$default &&
	            entry._state !== lib$es6$promise$$internal$$PENDING) {
	          this._settledAt(entry._state, i, entry._result);
	        } else if (typeof then !== 'function') {
	          this._remaining--;
	          this._result[i] = entry;
	        } else if (c === lib$es6$promise$promise$$default) {
	          var promise = new c(lib$es6$promise$$internal$$noop);
	          lib$es6$promise$$internal$$handleMaybeThenable(promise, entry, then);
	          this._willSettleAt(promise, i);
	        } else {
	          this._willSettleAt(new c(function(resolve) { resolve(entry); }), i);
	        }
	      } else {
	        this._willSettleAt(resolve(entry), i);
	      }
	    };
	
	    lib$es6$promise$enumerator$$Enumerator.prototype._settledAt = function(state, i, value) {
	      var promise = this.promise;
	
	      if (promise._state === lib$es6$promise$$internal$$PENDING) {
	        this._remaining--;
	
	        if (state === lib$es6$promise$$internal$$REJECTED) {
	          lib$es6$promise$$internal$$reject(promise, value);
	        } else {
	          this._result[i] = value;
	        }
	      }
	
	      if (this._remaining === 0) {
	        lib$es6$promise$$internal$$fulfill(promise, this._result);
	      }
	    };
	
	    lib$es6$promise$enumerator$$Enumerator.prototype._willSettleAt = function(promise, i) {
	      var enumerator = this;
	
	      lib$es6$promise$$internal$$subscribe(promise, undefined, function(value) {
	        enumerator._settledAt(lib$es6$promise$$internal$$FULFILLED, i, value);
	      }, function(reason) {
	        enumerator._settledAt(lib$es6$promise$$internal$$REJECTED, i, reason);
	      });
	    };
	    function lib$es6$promise$polyfill$$polyfill() {
	      var local;
	
	      if (typeof global !== 'undefined') {
	          local = global;
	      } else if (typeof self !== 'undefined') {
	          local = self;
	      } else {
	          try {
	              local = Function('return this')();
	          } catch (e) {
	              throw new Error('polyfill failed because global object is unavailable in this environment');
	          }
	      }
	
	      var P = local.Promise;
	
	      if (P && Object.prototype.toString.call(P.resolve()) === '[object Promise]' && !P.cast) {
	        return;
	      }
	
	      local.Promise = lib$es6$promise$promise$$default;
	    }
	    var lib$es6$promise$polyfill$$default = lib$es6$promise$polyfill$$polyfill;
	
	    var lib$es6$promise$umd$$ES6Promise = {
	      'Promise': lib$es6$promise$promise$$default,
	      'polyfill': lib$es6$promise$polyfill$$default
	    };
	
	    /* global define:true module:true window: true */
	    if ("function" === 'function' && __webpack_require__(6)['amd']) {
	      !(__WEBPACK_AMD_DEFINE_RESULT__ = function() { return lib$es6$promise$umd$$ES6Promise; }.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	    } else if (typeof module !== 'undefined' && module['exports']) {
	      module['exports'] = lib$es6$promise$umd$$ES6Promise;
	    } else if (typeof this !== 'undefined') {
	      this['ES6Promise'] = lib$es6$promise$umd$$ES6Promise;
	    }
	
	    lib$es6$promise$polyfill$$default();
	}).call(this);
	
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3), (function() { return this; }()), __webpack_require__(4)(module)))

/***/ },
/* 3 */
/***/ function(module, exports) {

	// shim for using process in browser
	
	var process = module.exports = {};
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;
	
	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}
	
	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = setTimeout(cleanUpNextTick);
	    draining = true;
	
	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    clearTimeout(timeout);
	}
	
	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        setTimeout(drainQueue, 0);
	    }
	};
	
	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};
	
	function noop() {}
	
	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	
	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};
	
	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ },
/* 5 */
/***/ function(module, exports) {

	/* (ignored) */

/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = function() { throw new Error("define cannot be used indirect"); };


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/*****************************************************************
	  * Delayed: A collection of setTimeout-related function wranglers
	  * Copyright (c) Rod Vagg (@rvagg) 2014
	  * https://github.com/rvagg/delayed
	  * License: MIT
	  */
	
	!(function init (name, definition) {
	  if (typeof module != 'undefined' && module.exports) module.exports = definition()
	  else if (true) __webpack_require__(6)(name, definition)
	  else this[name] = definition()
	}('delayed', function setup () {
	
	  var context = this
	    , old     = context.delayed
	    , deferMs = 1
	
	
	  function slice (arr, i) {
	    return Array.prototype.slice.call(arr, i)
	  }
	
	
	  function delay (fn, ms, ctx) {
	    var args = slice(arguments, 3)
	    return setTimeout(function delayer () {
	      fn.apply(ctx || null, args)
	    }, ms)
	  }
	
	
	  function defer (fn, ctx) {
	    return delay.apply(null, [ fn, deferMs, ctx ].concat(slice(arguments, 2)))
	  }
	
	
	  function delayed () {
	    var args = slice(arguments)
	    return function delayeder () {
	      return delay.apply(null, args.concat(slice(arguments)))
	    }
	  }
	
	
	  function deferred (fn, ctx) {
	    return delayed.apply(null, [ fn, deferMs, ctx ].concat(slice(arguments, 2)))
	  }
	
	
	  function cumulativeDelayed (fn, ms, ctx) {
	    var args = slice(arguments, 3)
	      , timeout = null
	
	    return function cumulativeDelayeder () {
	      var _args = slice(arguments)
	        , f = function cumulativeDelayedCaller () {
	            return fn.apply(ctx || null, args.concat(_args))
	          }
	      if (timeout != null)
	        clearTimeout(timeout)
	      return timeout = setTimeout(f, ms)
	    }
	  }
	
	
	  function noConflict () {
	    context.delayed = old
	    return this
	  }
	
	
	  return {
	      delay             : delay
	    , defer             : defer
	    , delayed           : delayed
	    , deferred          : deferred
	    , noConflict        : noConflict
	    , cumulativeDelayed : cumulativeDelayed
	    , debounce          : cumulativeDelayed
	  }
	
	}));


/***/ },
/* 8 */
/***/ function(module, exports) {

	module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" style=\"display:none\"><symbol id=\"izyconnect-icon-check\" viewbox=\"0 0 15 15\"><path d=\"M1.535 7.469l4.361 5.956 7.603-11.487\"/></symbol><symbol id=\"izyconnect-icon-error\" viewbox=\"0 0 15 15\"><path d=\"M.619 13.4L7.5 1.482 14.381 13.4zM7.5 5.303v4.875M7.5 10.959v.781\"/></symbol><symbol id=\"izyconnect-icon-loading\" viewbox=\"0 0 15 15\"><path d=\"M13.499 7.5c0 5.472-7.059 8.048-10.586 3.865-3.55-4.21.257-10.749 5.666-9.767 2.814.511 4.92 3.043 4.92 5.902z\"/></symbol><symbol id=\"izyconnect-icon-next\" viewbox=\"0 0 15 15\"><path stroke-miterlimit=\"10\" d=\"M5.342 1.505L10.658 7.5l-5.316 5.906\"/></symbol><symbol id=\"izyconnect-icon-tooltip-info\" viewbox=\"0 0 15 15\"><path d=\"M7.5 1.513c-3.288 0-5.943 2.655-5.943 5.943s2.655 5.943 5.943 5.943 5.943-2.655 5.943-5.943S10.788 1.513 7.5 1.513zm.938 9.244a.552.552 0 0 1-.541.541h-.82a.552.552 0 0 1-.541-.541v-3.46c0-.304.251-.541.541-.541h.819c.291 0 .541.251.541.541v3.46zm-.951-5.41c-.608 0-1.109-.502-1.109-1.123s.502-1.123 1.109-1.123a1.122 1.122 0 1 1 0 2.246z\"/></symbol><symbol id=\"izyconnect-logo-slim\" viewbox=\"0 0 12.673 15\"><circle cx=\"2.051\" cy=\"4.113\" r=\".497\"/><path d=\"M12.22 9.732c-.005-.182-.011-.354-.011-.517l-.001-.378-.001-2.823a.406.406 0 0 0-.812 0l.001.969V8.83a.96.96 0 0 1-1.918-.022V6.035a.406.406 0 0 0-.812 0v2.772c0 .976.795 1.771 1.771 1.771.363 0 .701-.11.982-.298.004.705-.059 1.42-.467 1.89-.741.852-2.055.372-3.338-.194-.237-.104-.477-.215-.709-.322l-.312-.144a7.406 7.406 0 0 0-.738-.288l.171-.362.268-.567c.292-.619.586-1.249.862-1.842l.43-.919.314-.67c.062-.137.126-.273.19-.41l.124-.265a.407.407 0 0 0-.368-.578H4.751a.406.406 0 0 0-.406.406V8.82a.942.942 0 0 1-1.882 0V6.005a.407.407 0 0 0-.684-.296L.491 6.916a.406.406 0 1 0 .556.592l.604-.566V8.82c0 .967.787 1.753 1.754 1.753s1.754-.786 1.754-1.753V6.422h2.049l-.045.096-.314.669c-.144.307-.288.614-.43.921-.276.592-.569 1.221-.86 1.838l-.268.568-.234.497-.125-.024c-.387-.07-1.108-.199-1.663.221a1.093 1.093 0 0 0-.426.738.927.927 0 0 0 .215.706c.452.532 1.216.474 1.706.149.336-.223.565-.541.732-.842.256.079.51.176.759.29l.31.142c.236.109.479.221.722.328.649.286 1.634.72 2.576.72.623 0 1.227-.19 1.702-.737.72-.826.684-2.015.655-2.97zm-7.905 2.393c-.198.131-.502.163-.639.002a.11.11 0 0 1-.027-.091.277.277 0 0 1 .109-.179c.129-.098.301-.131.487-.131.135 0 .278.017.417.04a1.305 1.305 0 0 1-.347.359z\"/></symbol></svg>\n\n<!-- btn start -->\n<button class=\"izyconnect-btn-start\">\n\t<svg class=\"izyconnect-btn-start__logo\" ><use xlink:href=\"#izyconnect-logo-slim\" /></svg>\n\tSe connecter avec Izyfill\n</button>\n\n\n<!-- label -->\n<div class=\"izyconnect-label\" data-step=\"\">\n    <span class=\"izyconnect-label__text\"></span>\n\t<div class=\"izyconnect-label__nav\">\n\t\t<span class=\"izyconnect-label__nav__item\" data-target=\"1\" data-item=\"email\">1. Email</span>\n\t\t<span class=\"izyconnect-label__nav__item\" data-target=\"2\" data-item=\"prenom\">2. Prénom</span>\n\t\t<span class=\"izyconnect-label__nav__item\" data-target=\"3\" data-item=\"nom\">3. Nom</span>\n\t</div>\n</div>\n\n<!-- tooltip -->\n<div class=\"izyconnect-tooltip\" data-status=\"\">\n\t<p class=\"izyconnect-tooltip__text\"></p>\n\n\t<svg class=\"izyconnect-tooltip__icon -error\"><use xlink:href=\"#izyconnect-icon-tooltip-info\" /></svg>\n\t<svg class=\"izyconnect-tooltip__icon -info\"><use xlink:href=\"#izyconnect-icon-tooltip-info\" /></svg>\n</div>\n\n\n<!-- input gsm-->\n<div class=\"izyconnect-input-gsm is-visible\" data-progress=\"\">\n\t<input type=\"tel\" class=\"izyconnect-input-gsm__input\" maxlength=\"14\" placeholder=\"0_ __ __ __ __\">\n\n\t<button class=\"izyconnect-input__submit\">\n\t\t<svg class=\"izyconnect-input__icon -next\"><use xlink:href=\"#izyconnect-icon-next\" /></svg>\n\t\t<svg class=\"izyconnect-input__icon -progress\"><use xlink:href=\"#izyconnect-icon-loading\" /></svg>\n\t</button>\n</div>\n\n\n<!-- input code-->\n<div class=\"izyconnect-input-code\" data-progress=\"\">\n\t<input type=\"text\" class=\"izyconnect-input-code__input\" maxlength=\"6\" placeholder=\"______\">\n\n\t<button class=\"izyconnect-input__submit\">\n\t\t<svg class=\"izyconnect-input__icon -next\"><use xlink:href=\"#izyconnect-icon-next\" /></svg>\n\t\t<svg class=\"izyconnect-input__icon -progress\"><use xlink:href=\"#izyconnect-icon-loading\" /></svg>\n\t</button>\n</div>\n\n\n<!-- input subscribe-->\n<div class=\"izyconnect-input-subscribe\" data-step=\"1\" data-progress=\"\" data-showSubmit=\"false\">\n\t<input type=\"text\" class=\"izyconnect-input-subscribe__input -prenom\" maxlength=\"25\" placeholder=\"Votre prénom\">\n\t<input type=\"text\" class=\"izyconnect-input-subscribe__input -nom\" maxlength=\"25\" placeholder=\"Votre nom\">\n\t<input type=\"text\" class=\"izyconnect-input-subscribe__input -email\" maxlength=\"30\" placeholder=\"Votre email\">\n\n\t<button class=\"izyconnect-input__submit\">\n\t\t<svg class=\"izyconnect-input__icon -next\"><use xlink:href=\"#izyconnect-icon-next\" /></svg>\n\t\t<svg class=\"izyconnect-input__icon -progress\"><use xlink:href=\"#izyconnect-icon-loading\" /></svg>\n\t</button>\n</div>\n\n\n\n<!-- status-->\n<div class=\"izyconnect-status-wrapper\">\n\t<p class=\"izyconnect-status\" data-status=\"loading\">\n\t\t<span class=\"izyconnect-status__text\">qsdqsd</span>\n\t\t<!--<span class=\"izyconnect-status__icon\"></span>-->\n\t\t<svg class=\"izyconnect-status__icon -loading\" ><use xlink:href=\"#izyconnect-icon-loading\" /></svg>\n\t\t<svg class=\"izyconnect-status__icon -check\" ><use xlink:href=\"#izyconnect-icon-check\" /></svg>\n\t\t<svg class=\"izyconnect-status__icon -error\"><use xlink:href=\"#izyconnect-icon-error\" /></svg>\n\t</p>\n</div>\n\n\n<!-- progress bar -->\n<div class=\"izyconnect-progressbar\" data-steps=\"3\" data-progress=\"\"></div>";

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(10);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(12)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../../node_modules/css-loader/index.js!./style.css", function() {
				var newContent = require("!!./../../../node_modules/css-loader/index.js!./style.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(11)();
	// imports
	
	
	// module
	exports.push([module.id, "@charset \"UTF-8\";.izyconnect-status-wrapper{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.izyconnect-label,.izyconnect-status__text{max-width:100%;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}.izyconnect-btn-start{overflow:visible;background-color:transparent;border:0;text-transform:inherit;-webkit-appearance:button;cursor:pointer}.izyconnect-btn-start::-moz-focus-inner{padding:0;border:0}.izyconnect-button{width:15em;height:3em;font-family:sans-serif;font-size:18px;position:relative;overflow:visible}.izyconnect-button.is-active{border-top:1px solid #e6e6e6}.izyconnect-button>*{font-size:inherit;font-family:inherit;box-sizing:border-box;margin:0}.izyconnect-btn-start{z-index:7;background-color:#0099a8;color:#fff;-moz-text-align-last:left;text-align-last:left;border-radius:5px;padding:0;margin:0;line-height:3.45em;position:absolute;top:0;left:0;width:100%;height:100%;transition:background-color .3s cubic-bezier(.25,.46,.45,.94)}.izyconnect-btn-start.fade-out{opacity:0;transition:opacity .3s cubic-bezier(.25,.46,.45,.94)}.izyconnect-btn-start.is-hidden{display:none}.izyconnect-btn-start:hover{background-color:#1aa3b1}.izyconnect-btn-start__logo{fill:#fff;height:100%;display:block;float:left;vertical-align:middle;width:10%;padding:0 .5em;margin-right:.5em;text-align:center;border:1px solid transparent;border-right:1px solid #1aa3b1}.izyconnect-label{z-index:3;position:absolute;top:0;left:0;width:100%;font-size:.7em;opacity:0;transition:all .3s cubic-bezier(.25,.46,.45,.94);padding-bottom:.3em}.izyconnect-label.is-visible{opacity:1;-webkit-transform:translateY(-100%);transform:translateY(-100%)}.izyconnect-label__nav__item,[data-step=\"1\"] .izyconnect-label__nav__item:nth-child(2),[data-step=\"1\"] .izyconnect-label__nav__item:nth-child(3),[data-step=\"2\"] .izyconnect-label__nav__item:nth-child(3){font-size:.8em;color:#999}[data-step=\"2\"] .izyconnect-label__nav__item:nth-child(1):before,[data-step=\"3\"] .izyconnect-label__nav__item:nth-child(1):before,[data-step=\"3\"] .izyconnect-label__nav__item:nth-child(2):before{content:\" \\2713   \";color:#0099a8;text-decoration:none!important}[data-step=\"2\"] .izyconnect-label__nav__item:hover:nth-child(1),[data-step=\"3\"] .izyconnect-label__nav__item:hover:nth-child(1),[data-step=\"3\"] .izyconnect-label__nav__item:hover:nth-child(2){text-decoration:none;color:#000;cursor:pointer}[data-step=\"1\"] .izyconnect-label__nav__item:nth-child(1),[data-step=\"2\"] .izyconnect-label__nav__item:nth-child(2),[data-step=\"3\"] .izyconnect-label__nav__item:nth-child(3){color:#000;font-size:1em}.izyconnect-label__text{display:inline-block}.izyconnect-label__nav,[data-has-nav=true] .izyconnect-label__text{display:none}[data-has-nav=true] .izyconnect-label__nav{display:block}.izyconnect-label__nav__item{margin-right:.8em}.izyconnect-progressbar{z-index:3;display:block;position:absolute;top:100%;left:0;width:100%;height:5px;background-color:#e6e6e6;border-top:2px solid #fff;border-bottom:2px solid #fff;transition:opacity .3s cubic-bezier(.25,.46,.45,.94);opacity:0}.izyconnect-progressbar.is-visible{opacity:1}.izyconnect-progressbar:after,.izyconnect-progressbar:before{content:\"\";display:block;position:absolute;top:0;left:0;width:100%;height:100%}.izyconnect-progressbar:before{z-index:2;background-image:linear-gradient(90deg,transparent 32%,#fff 0,#fff 33%,transparent 0,transparent 65%,#fff 0,#fff 66%,transparent 0,transparent)}.izyconnect-progressbar[data-steps='6']:before{background-image:linear-gradient(90deg,transparent 10%,#fff 0,#fff 11%,transparent 0,transparent 21%,#fff 0,#fff 22%,transparent 0,transparent 32%,#fff 0,#fff 33%,transparent 0,transparent 65%,#fff 0,#fff 66%,transparent 0,transparent)}.izyconnect-progressbar:after{z-index:1;background-color:#0099a8;transition:width .3s cubic-bezier(.25,.46,.45,.94);width:0}.izyconnect-progressbar[data-progress='1/3/3']:after{width:11%}.izyconnect-progressbar[data-progress='2/3/3']:after{width:22%}.izyconnect-progressbar[data-progress='1/3']:after,.izyconnect-progressbar[data-progress='3/3/3']:after{width:32%}.izyconnect-progressbar[data-progress='2/3']:after{width:65%}.izyconnect-progressbar[data-progress='3/3']:after{width:100%}.izyconnect-status-wrapper{display:block;position:absolute;top:0;left:0;width:100%;height:100%;color:#fff;overflow:hidden;z-index:1}.izyconnect-status-wrapper.is-visible{z-index:8}.izyconnect-status{-webkit-transform:scale(1);transform:scale(1);transition:all .3s cubic-bezier(.25,.46,.45,.94);margin:0;opacity:0;background-color:#0099a8;display:block;position:relative;width:100%;height:100%}.izyconnect-status.animate-in{opacity:1;-webkit-transform:scale(1);transform:scale(1)}.izyconnect-status[data-status=error]{background-color:#d90000}.izyconnect-status__text{line-height:3.3em;display:block;text-indent:1em;padding-right:3em}.izyconnect-status__icon{position:absolute;top:0;right:0;border:.45em solid #0099a8;background-color:#0099a8;width:2.1em;height:2.1em;stroke-width:1px;stroke:#fff;fill:none;stroke-linecap:round;stroke-linejoin:round;display:none}.izyconnect-status__icon.-loading{stroke-dasharray:37.52;stroke-dashoffset:0;opacity:0;-webkit-animation-fill-mode:forwards;animation-fill-mode:forwards;-webkit-animation-direction:normal;animation-direction:normal;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-name:a;animation-name:a;-webkit-animation-duration:1.5s;animation-duration:1.5s;-webkit-animation-timing-function:cubic-bezier(.25,.46,.45,.94);animation-timing-function:cubic-bezier(.25,.46,.45,.94)}.izyconnect-status[data-status=loading] .izyconnect-status__icon.-loading{display:block}.izyconnect-status__icon.-check{stroke-dasharray:21.27px;stroke-dashoffset:21.27px;opacity:0;-webkit-animation-fill-mode:forwards;animation-fill-mode:forwards;-webkit-animation-direction:normal;animation-direction:normal;-webkit-animation-iteration-count:1;animation-iteration-count:1;-webkit-animation-name:b;animation-name:b;-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-timing-function:cubic-bezier(.25,.46,.45,.94);animation-timing-function:cubic-bezier(.25,.46,.45,.94)}.izyconnect-status[data-status=check] .izyconnect-status__icon.-check{display:block}.izyconnect-status__icon.-error{border-color:#d90000;background-color:#d90000;stroke-dasharray:46.94px;stroke-dashoffset:46.94px;opacity:0;-webkit-animation-fill-mode:forwards;animation-fill-mode:forwards;-webkit-animation-direction:normal;animation-direction:normal;-webkit-animation-iteration-count:1;animation-iteration-count:1;-webkit-animation-name:b;animation-name:b;-webkit-animation-duration:2s;animation-duration:2s;-webkit-animation-timing-function:cubic-bezier(.25,.46,.45,.94);animation-timing-function:cubic-bezier(.25,.46,.45,.94)}.izyconnect-status[data-status=error] .izyconnect-status__icon.-error{display:block}@-webkit-keyframes a{0%{stroke-dashoffset:37.52px;opacity:0}50%{opacity:1;stroke-dashoffset:0}to{opacity:0;stroke-dashoffset:-37.52px}}@keyframes a{0%{stroke-dashoffset:37.52px;opacity:0}50%{opacity:1;stroke-dashoffset:0}to{opacity:0;stroke-dashoffset:-37.52px}}@-webkit-keyframes b{to{opacity:1;stroke-dashoffset:0}}@keyframes b{to{opacity:1;stroke-dashoffset:0}}.izyconnect-input-code,.izyconnect-input-gsm,.izyconnect-input-subscribe{width:100%;height:100%;display:none}.is-visible.izyconnect-input-code,.is-visible.izyconnect-input-gsm,.is-visible.izyconnect-input-subscribe{display:block}.izyconnect-input-code input[type=text],.izyconnect-input-gsm input[type=tel],.izyconnect-input-subscribe input[type=text]{border:0;margin:0;padding:0;display:block;height:100%;width:100%;background-color:transparent;color:inherit;text-indent:.5em;font-family:inherit;font-size:1.3em;display:inline-block;vertical-align:baseline;-webkit-appearance:none;-moz-appearance:none;appearance:none;transition:all .3s ease-out;border-radius:0;position:absolute;top:0;left:0;padding-top:.2em;margin-bottom:-.2em}.izyconnect-input-code input[type=text]::-webkit-input-placeholder,.izyconnect-input-gsm input[type=tel]::-webkit-input-placeholder,.izyconnect-input-subscribe input[type=text]::-webkit-input-placeholder{color:#e6e6e6}.izyconnect-input-code input[type=text]::-moz-placeholder,.izyconnect-input-gsm input[type=tel]::-moz-placeholder,.izyconnect-input-subscribe input[type=text]::-moz-placeholder{color:#e6e6e6}.izyconnect-input-code input[type=text]:-ms-input-placeholder,.izyconnect-input-gsm input[type=tel]:-ms-input-placeholder,.izyconnect-input-subscribe input[type=text]:-ms-input-placeholder{color:#e6e6e6}.izyconnect-input-code input[type=text]::placeholder,.izyconnect-input-gsm input[type=tel]::placeholder,.izyconnect-input-subscribe input[type=text]::placeholder{color:#e6e6e6}.izyconnect-input-code input[type=text]:focus,.izyconnect-input-gsm input[type=tel]:focus,.izyconnect-input-subscribe input[type=text]:focus{outline:none!important;outline:0}.izyconnect-input-code input[disabled][type=text],.izyconnect-input-gsm input[disabled][type=tel],.izyconnect-input-subscribe input[disabled][type=text]{opacity:.4;cursor:not-allowed}.izyconnect-input-code input[type=text]::-moz-focus-inner,.izyconnect-input-gsm input[type=tel]::-moz-focus-inner,.izyconnect-input-subscribe input[type=text]::-moz-focus-inner{border:0;padding:0}.izyconnect-input-code input[type=text]::-ms-clear,.izyconnect-input-gsm input[type=tel]::-ms-clear,.izyconnect-input-subscribe input[type=text]::-ms-clear{display:none}.izyconnect-input__submit{overflow:visible;background-color:transparent;border:0;text-transform:inherit;font-family:inherit;-webkit-appearance:button;cursor:pointer;z-index:6;position:absolute;top:.1em;right:0;width:20%;height:100%;-webkit-transform:scale(.8);transform:scale(.8);transition:-webkit-transform .2s cubic-bezier(.25,.46,.45,.94);transition:transform .2s cubic-bezier(.25,.46,.45,.94);transition:transform .2s cubic-bezier(.25,.46,.45,.94),-webkit-transform .2s cubic-bezier(.25,.46,.45,.94)}.izyconnect-input__submit::-moz-focus-inner{padding:0;border:0}.izyconnect-input__submit:hover{cursor:default}.izyconnect-input__icon{position:absolute;top:0;right:0;background-color:transparent;stroke-width:1px;fill:transparent;stroke-linecap:round;stroke-linejoin:round;display:block;width:100%;height:100%;-webkit-transform-origin:center center;transform-origin:center center;border:1px solid transparent!important}.izyconnect-input__icon.-progress{border:.45em solid transparent;z-index:5;stroke:#0099a8;stroke-dasharray:37.52;stroke-dashoffset:37.52;transition:stroke-dashoffset .3s cubic-bezier(.25,.46,.45,.94);-webkit-transform:scale(.8);transform:scale(.8)}.izyconnect-input__icon.-next{z-index:6;border:.9em solid transparent;stroke:#fff;-webkit-transform:scale(.5);transform:scale(.5);opacity:0}.izyconnect-input-code,.izyconnect-input-code input[type=text]{z-index:4}.izyconnect-input-code[data-progress=\"1\"] .izyconnect-input__icon.-progress{stroke-dashoffset:30.02}.izyconnect-input-code[data-progress=\"2\"] .izyconnect-input__icon.-progress{stroke-dashoffset:22.51}.izyconnect-input-code[data-progress=\"3\"] .izyconnect-input__icon.-progress{stroke-dashoffset:15.01}.izyconnect-input-code[data-progress=\"4\"] .izyconnect-input__icon.-progress{stroke-dashoffset:7.5}.izyconnect-input-code[data-progress=\"5\"] .izyconnect-input__icon.-progress{-webkit-transform:scale(1);transform:scale(1);stroke-dashoffset:0;fill:#0099a8;transition:fill .3s cubic-bezier(.25,.46,.45,.94),-webkit-transform .3s cubic-bezier(.25,.46,.45,.94);transition:fill .3s cubic-bezier(.25,.46,.45,.94),transform .3s cubic-bezier(.25,.46,.45,.94);transition:fill .3s cubic-bezier(.25,.46,.45,.94),transform .3s cubic-bezier(.25,.46,.45,.94),-webkit-transform .3s cubic-bezier(.25,.46,.45,.94)}.izyconnect-input-code[data-progress=\"7\"] .izyconnect-input__submit:hover{-webkit-transform:scale(.9);transform:scale(.9);cursor:pointer}.izyconnect-input-code[data-progress=\"7\"] .izyconnect-input__icon.-next{opacity:.8}.izyconnect-input-gsm,.izyconnect-input-gsm input[type=tel]{z-index:4}.izyconnect-input-gsm[data-progress=\"1\"] .izyconnect-input__icon.-progress{stroke-dashoffset:34.11}.izyconnect-input-gsm[data-progress=\"2\"] .izyconnect-input__icon.-progress{stroke-dashoffset:30.7}.izyconnect-input-gsm[data-progress=\"3\"] .izyconnect-input__icon.-progress{stroke-dashoffset:27.29}.izyconnect-input-gsm[data-progress=\"4\"] .izyconnect-input__icon.-progress{stroke-dashoffset:23.88}.izyconnect-input-gsm[data-progress=\"5\"] .izyconnect-input__icon.-progress{stroke-dashoffset:20.47}.izyconnect-input-gsm[data-progress=\"6\"] .izyconnect-input__icon.-progress{stroke-dashoffset:17.05}.izyconnect-input-gsm[data-progress=\"7\"] .izyconnect-input__icon.-progress{stroke-dashoffset:13.64}.izyconnect-input-gsm[data-progress=\"8\"] .izyconnect-input__icon.-progress{stroke-dashoffset:10.23}.izyconnect-input-gsm[data-progress=\"9\"] .izyconnect-input__icon.-progress{stroke-dashoffset:6.82}.izyconnect-input-gsm[data-progress=\"10\"] .izyconnect-input__icon.-progress{stroke-dashoffset:3.41}.izyconnect-input-gsm[data-progress=\"11\"] .izyconnect-input__icon.-progress{-webkit-transform:scale(1);transform:scale(1);stroke-dashoffset:0;fill:#0099a8;transition:fill .3s cubic-bezier(.25,.46,.45,.94),-webkit-transform .3s cubic-bezier(.25,.46,.45,.94);transition:fill .3s cubic-bezier(.25,.46,.45,.94),transform .3s cubic-bezier(.25,.46,.45,.94);transition:fill .3s cubic-bezier(.25,.46,.45,.94),transform .3s cubic-bezier(.25,.46,.45,.94),-webkit-transform .3s cubic-bezier(.25,.46,.45,.94)}.izyconnect-input-gsm[data-progress=\"11\"] .izyconnect-input__submit:hover{-webkit-transform:scale(.9);transform:scale(.9);cursor:pointer}.izyconnect-input-gsm[data-progress=\"11\"] .izyconnect-input__icon.-next{opacity:.8}.izyconnect-input-subscribe{z-index:4}.izyconnect-input-subscribe input[type=text]{z-index:4;display:none}.izyconnect-input-subscribe__input{border:1px solid red}.izyconnect-input-subscribe[data-step=\"1\"] .izyconnect-input-subscribe__input.-email,.izyconnect-input-subscribe[data-step=\"2\"] .izyconnect-input-subscribe__input.-prenom,.izyconnect-input-subscribe[data-step=\"3\"] .izyconnect-input-subscribe__input.-nom{display:block}.izyconnect-input-subscribe[data-showsubmit=true] .izyconnect-input__icon.-progress{-webkit-transform:scale(1);transform:scale(1);stroke-dashoffset:0;fill:#0099a8;transition:fill .3s cubic-bezier(.25,.46,.45,.94),-webkit-transform .3s cubic-bezier(.25,.46,.45,.94);transition:fill .3s cubic-bezier(.25,.46,.45,.94),transform .3s cubic-bezier(.25,.46,.45,.94);transition:fill .3s cubic-bezier(.25,.46,.45,.94),transform .3s cubic-bezier(.25,.46,.45,.94),-webkit-transform .3s cubic-bezier(.25,.46,.45,.94)}.izyconnect-input-subscribe[data-showsubmit=true] .izyconnect-input__submit:hover{-webkit-transform:scale(.9);transform:scale(.9);cursor:pointer}.izyconnect-input-subscribe[data-showsubmit=true] .izyconnect-input__icon.-next{opacity:.8}.izyconnect-tooltip{z-index:2;position:absolute;top:100%;left:0;width:100%;height:auto;background-color:#f2f2f2;margin-top:2px;font-size:.7em;color:#000;-webkit-transform:translateY(-100%);transform:translateY(-100%);transition:opacity .05s ease,-webkit-transform .3s cubic-bezier(.25,.46,.45,.94);transition:transform .3s cubic-bezier(.25,.46,.45,.94),opacity .05s ease;transition:transform .3s cubic-bezier(.25,.46,.45,.94),opacity .05s ease,-webkit-transform .3s cubic-bezier(.25,.46,.45,.94);opacity:0;display:block}.izyconnect-tooltip.animate-in{opacity:1;-webkit-transform:translateY(0);transform:translateY(0)}.izyconnect-tooltip__text{margin:0;padding:.9em .9em .9em 2em;line-height:1.2}.izyconnect-tooltip__icon{position:absolute;top:50%;left:0;-webkit-transform:translateX(-50%) translateY(-50%);transform:translateX(-50%) translateY(-50%);width:1.6em;height:1.6em;background-color:#fff;border-radius:100%;stroke-width:0;border:1px solid #fff;display:none}.izyconnect-tooltip__icon.-error{fill:#d90000;-webkit-transform:translateX(-50%) translateY(-50%) rotate(180deg);transform:translateX(-50%) translateY(-50%) rotate(180deg)}.izyconnect-tooltip[data-status=error] .izyconnect-tooltip__icon.-error{display:block}.izyconnect-tooltip__icon.-info{fill:#0099a8}.izyconnect-tooltip[data-status=info] .izyconnect-tooltip__icon.-info{display:block}", ""]);
	
	// exports


/***/ },
/* 11 */
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	
	/* IZYCONNECT STATUS */
	
	module.exports = (function(){
	  'use strict';
	
	  var moduleElementWrapper = document.getElementsByClassName("izyconnect-status-wrapper")[0];
	  var moduleElement = moduleElementWrapper.getElementsByClassName("izyconnect-status")[0];
	  var moduleElementText = moduleElement.getElementsByClassName("izyconnect-status__text")[0];
	  var transitionend = __webpack_require__(14);
	
	
	  /**
	   * init
	   * @return {[type]} [description]
	   */
	  var init = function(){
	    //console.log(prefix('animation-end'));
	  };
	
	
	
	  /**
	   * animation
	   * @return {[type]} [description]
	   */
	  var animateIn = function(callback){
	
	    moduleElementWrapper.classList.add("is-visible");
	    moduleElement.classList.add("animate-in");
	
	    moduleElement.addEventListener("transitionend", fireAfterTransition, false);
	
	    //callback transition end
	    function fireAfterTransition(){
	
	      //if we have a special callback
	      if(callback !== undefined){
	        callback();
	      }
	
	      // remove event listener
	      moduleElement.removeEventListener('transitionend', fireAfterTransition, false);
	    }
	  };
	
	
	  var animateOut = function(callback){
	    moduleElement.classList.remove("animate-in");
	    moduleElement.addEventListener("transitionend", fireAfterTransition, false);
	
	    //calback transition end
	    function fireAfterTransition(){
	      //if we have a special callback
	      if(callback !== undefined){
	        callback();
	      }
	
	      moduleElementWrapper.classList.remove("is-visible");
	      moduleElement.removeEventListener('transitionend', fireAfterTransition, false);
	    }
	  };
	
	
	
	  /**
	   * setMessage
	   * @return {[type]} [description]
	   */
	  var setMessage = function(message){
	    moduleElementText.textContent = message;
	  };
	
	
	  /**
	   * setStatus
	   * @return {[type]} [description]
	   */
	  var setStatus = function(status){
	    moduleElement.dataset.status = status;
	  };
	
	
	
	
	  return {
	    init: init,
	    element: function() { return moduleElement; },
	    animateIn: animateIn,
	    animateOut: animateOut,
	    setMessage: setMessage,
	    setStatus: setStatus
	  };
	
	})();

/***/ },
/* 14 */
/***/ function(module, exports) {

	/**
	 * Transition-end mapping
	 */
	
	var map = {
	  'WebkitTransition' : 'webkitTransitionEnd',
	  'MozTransition' : 'transitionend',
	  'OTransition' : 'oTransitionEnd',
	  'msTransition' : 'MSTransitionEnd',
	  'transition' : 'transitionend'
	};
	
	/**
	 * Expose `transitionend`
	 */
	
	var el = document.createElement('p');
	
	for (var transition in map) {
	  if (null != el.style[transition]) {
	    module.exports = map[transition];
	    break;
	  }
	}


/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	
	/* IZYCONNECT LABEL */
	
	module.exports = (function(){
	  'use strict';
	
	  var moduleElement = document.getElementsByClassName("izyconnect-label")[0];
	  var moduleElementText = moduleElement.getElementsByClassName("izyconnect-label__text")[0];
	  var moduleElementNav = moduleElement.getElementsByClassName("izyconnect-label__nav")[0];
	  var transitionend = __webpack_require__(14);
	
	
	  /**
	   * init
	   * @return {[type]} [description]
	   */
	  var init = function(){
	    //console.log(prefix('animation-end'));
	  };
	
	
	
	  /**
	   * animation
	   * @return {[type]} [description]
	   */
	  var animateIn = function(callback){
	
	    moduleElement.classList.add("is-visible");
	    moduleElement.addEventListener("transitionend", fireAfterTransition, false);
	
	    //callback transition end
	    function fireAfterTransition(){
	
	      //if we have a special callback
	      if(callback !== undefined){
	        callback();
	      }
	
	      // remove event listener
	      moduleElement.removeEventListener('transitionend', fireAfterTransition, false);
	    }
	  };
	
	
	  var animateOut = function(callback){
	    moduleElement.classList.remove("is-visible");
	    moduleElement.addEventListener("transitionend", fireAfterTransition, false);
	
	    //callback transition end
	    function fireAfterTransition(){
	
	      //if we have a special callback
	      if(callback !== undefined){
	        callback();
	      }
	
	      // remove event listener
	      moduleElement.removeEventListener('transitionend', fireAfterTransition, false);
	    }
	  };
	
	
	  /**
	   * setMessage
	   * @return {[type]} [description]
	   */
	  var setMessage = function(message){
	    moduleElementText.textContent = message;
	  };
	
	
	  /**
	   * enableNav
	   * @return {[type]} [description]
	   */
	  var enableNav = function(){
	    moduleElement.dataset.hasNav = "true";
	  };
	
	
	  /**
	   * disableNav
	   * @return {[type]} [description]
	   */
	  var disableNav = function(){
	    moduleElement.dataset.hasNav = "";
	  };
	
	
	  /**
	   * setNavStep
	   * @return {[type]} [description]
	   */
	  var setNavStep = function(step){
	    moduleElement.dataset.step = step;
	  };
	
	
	
	
	  return {
	    init: init,
	    animateIn: animateIn,
	    animateOut: animateOut,
	    setMessage:setMessage,
	    enableNav:enableNav,
	    disableNav:disableNav,
	    setNavStep:setNavStep,
	    element: function() { return moduleElement; },
	    elementNav: function() { return moduleElementNav; }
	  };
	
	})();

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	
	
	
	module.exports = (function(){
	  'use strict';
	
	  var izyConnectLabel = __webpack_require__(15);
	  var izyConnectInputGsm = __webpack_require__(17);
	  var izyConnectProgressBar = __webpack_require__(19);
	
	  var moduleElement = document.getElementsByClassName("izyconnect-btn-start")[0];
	  var transitionend = __webpack_require__(14);
	
	
	
	  /**
	   * init
	   * @return {[type]} [description]
	   */
	/*
	  var init = function(){
	    _bindUiActions();
	  };
	*/
	
	
	
	  /**
	   * Bind ui Actions
	   * @return {[type]} [description]
	   */
	  var bindUiActions = function(){
	
	    moduleElement.addEventListener("click", _handleClick, false);
	    moduleElement.addEventListener("touch", _handleClick, false);
	  };
	
	
	
	  /**
	   * _handleClick
	   * @return {[type]} [description]
	   */
	  var _handleClick = function(){
	    //activate btn izyconnect (show progressbar)
	    document.getElementsByClassName("izyconnect-button")[0].classList.add("is-active");
	    izyConnectProgressBar.animateIn();
	
	    //fade out btn
	    _fadeOut();
	
	    //show label
	    //izyConnectLabel.animateIn(function(){console.log("hello from custom callback");});
	    izyConnectLabel.setMessage("Votre numéro de mobile");
	    izyConnectLabel.animateIn();
	
	    //set focus on input gsm
	    izyConnectInputGsm.elementInput().focus();
	  };
	
	
	
	
	  /**
	   * animation
	   * @return {[type]} [description]
	   */
	  var _fadeOut = function(){
	
	    //hide btn
	    moduleElement.classList.add("fade-out");
	    moduleElement.addEventListener("transitionend", fireAfterTransition, false);
	
	    //callback transition end
	    function fireAfterTransition(){
	
	      moduleElement.classList.add("is-hidden");
	      // remove event listener
	      moduleElement.removeEventListener('transitionend', fireAfterTransition, false);
	    }
	
	  };
	
	
	
	
	
	  /**
	   * hideElement
	   * @return {[type]} [description]
	   */
	  var showElement = function(){
	    //console.log("destroy " + moduleElement);
	    moduleElement.classList.remove("fade-out", "is-hidden");
	  };
	
	
	  /**
	   * destroyElement
	   * @return {[type]} [description]
	   */
	  var destroyElement = function(){
	    //console.log("destroy " + moduleElement);
	    moduleElement.remove();
	  };
	
	
	
	
	  return {
	    bindUiActions: bindUiActions,
	    element: function () { return moduleElement},
	    destroyElement: destroyElement,
	    showElement:showElement
	  };
	
	})();

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	
	/* IZYCONNECT INPUT GSM */
	
	module.exports = (function(){
	  'use strict';
	
	  //dependencies
	  var delayed = __webpack_require__(7);
	  //var keycode = require('keycode');
	
	  //modules
	  var izyConnectLabel = __webpack_require__(15);
	  var izyConnectTooltip = __webpack_require__(18);
	  var izyConnectStatus = __webpack_require__(13);
	  
	  //dom elements
	  var mElement = document.getElementsByClassName("izyconnect-input-gsm")[0];
	  var mElementInput = mElement.getElementsByClassName("izyconnect-input-gsm__input")[0];
	  var mElementSubmit = mElement.getElementsByClassName("izyconnect-input__submit")[0];
	
	  //variables
	  var sendDataToApi;
	  var gsmNumber;
	  var dataIsReadyToSubmit = false;
	
	
	
	  /**
	   * init
	   * @return {[type]} [description]
	   */
	  var init = function(sendDataFunction){
	    //store callback function
	    sendDataToApi = sendDataFunction;
	
	    //watch input
	    mElementInput.addEventListener("keyup", _watchInput, false);
	  };
	
	
	
	  /**
	   * _watchInput
	   * @return {[type]} [description]
	   */
	  var _watchInput = function(event) {
	
	    var lastKeyPressed = event.keyCode || event.which;
	
	    //if key is return and gsm is valid
	    if(lastKeyPressed === 13 && dataIsReadyToSubmit){_weHaveValidData();}
	
	    //format gsm number
	    _maskInput(lastKeyPressed);
	
	    //set progress
	    mElement.dataset.progress = gsmNumber.length;
	
	    if (gsmNumber.length >= 2) {
	      _validateInput(gsmNumber);
	    }
	
	  }
	
	
	
	  /**
	   * _maskInput
	   * @return {[type]} [description]
	   */
	  var _maskInput = function(lastKeyPressed) {
	
	    //remove non numeric characters and cache it
	    gsmNumber = mElementInput.value.toString().replace(/\D/g, '');
	
	    //if bacspace pressed -> we are deleting characters
	    //console.log(lastKeyPressed);
	    if(lastKeyPressed === 8
	    || lastKeyPressed === 46
	    || lastKeyPressed === 37
	    || lastKeyPressed === 39){ return;}
	
	    //split into array of two characters
	    var gsmPairs = gsmNumber.match(/[\s\S]{1,2}/g) || [];
	
	    //add space after two characters
	    gsmPairs = gsmPairs.map(function (element, index) {
	      //if we have two numbers in array && it's not the two last numbers
	      if (element.length === 2 && index !== 4) {
	        return element = element + " ";
	      }
	      else {
	        return element;
	      }
	    });
	
	    //return nicely formated gsm
	    mElementInput.value = gsmPairs.join("");
	  }
	
	
	  /**
	   * _validateInput
	   * @return {[type]} [description]
	   */
	  var _validateInput = function() {
	
	    if( _isGsmNumberValid() ){
	
	      //hide error
	      izyConnectTooltip.animateOut();
	
	      if (gsmNumber.length === 10) {
	        //bind click to submit btn
	        _initBtnSubmit();
	      }
	      else{
	        _disableBtnSubmit();
	      }
	    }
	    else{
	      //show info tooltip
	      izyConnectTooltip.setMessage("Veuillez entrer un numéro de mobile valide, commençant par 06 ou 07");
	      izyConnectTooltip.setStatus("error");
	      izyConnectTooltip.animateIn();
	    }
	
	  }
	
	
	
	  /**
	   * _isGsmNumberValid
	   * @return {[type]} [description]
	   */
	  var _isGsmNumberValid = function(){
	    //console.log(gsmNumber);
	    return gsmNumber.charAt(0) == 0 && (gsmNumber.charAt(1) == 7 || gsmNumber.charAt(1) == 6);
	  };
	
	
	
	
	  /**
	   * _weHaveValidData
	   * @return {[type]} [description]
	   */
	  var _weHaveValidData = function(){
	
	    //remove tooltip
	    izyConnectTooltip.animateOut();
	    izyConnectLabel.animateOut();
	
	    //set Status
	    izyConnectStatus.setMessage("Vérification du compte");
	    izyConnectStatus.setStatus("loading");
	    izyConnectStatus.animateIn();
	
	
	    //convertion au format international
	    //gsmNumber = gsmNumber
	
	    //sendDataToApi(gsmNumber);
	    delayed.delay(function () {
	      sendDataToApi(gsmNumber);
	    }, 500)
	
	  };
	
	
	
	
	  /**
	   * _initBtnSubmit
	   * @return {[type]} [description]
	   */
	  var _initBtnSubmit = function(){
	    //set progress
	    mElement.dataset.progress = 11;
	
	    //console.log("destroy " + moduleElement);
	    mElementSubmit.addEventListener("click", _weHaveValidData, false);
	    mElementSubmit.addEventListener("touch", _weHaveValidData, false);
	
	    dataIsReadyToSubmit = true;
	  };
	
	
	  /**
	   * _disableBtnSubmit
	   * @return {[type]} [description]
	   */
	  var _disableBtnSubmit = function(){
	    //console.log("destroy " + moduleElement);
	    mElementSubmit.removeEventListener("click", _weHaveValidData, false);
	    mElementSubmit.removeEventListener("touch", _weHaveValidData, false);
	
	    dataIsReadyToSubmit = false;
	  };
	
	
	
	
	  /**
	   * hideInput
	   * @return {[type]} [description]
	   */
	  var hideInput = function(){
	    mElement.classList.remove("is-visible");
	  };
	
	  /**
	   * resetInput
	   * @return {[type]} [description]
	   */
	  var resetInput = function(){
	    mElementInput.value = "";
	  };
	
	  /**
	   * destroyInput
	   * @return {[type]} [description]
	   */
	  var destroyInput = function(){
	    mElement.remove();
	  };
	
	
	
	
	
	
	
	  return {
	    init: init,
	    destroyInput:destroyInput,
	    hideInput: hideInput,
	    resetInput: resetInput,
	    element: function () { return mElement;},
	    elementInput: function () { return mElementInput;}
	
	  };
	
	})();

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	
	/* IZYCONNECT TOOLTIP */
	
	module.exports = (function(){
	  'use strict';
	
	  var moduleElement = document.getElementsByClassName("izyconnect-tooltip")[0];
	  var moduleElementText = moduleElement.getElementsByClassName("izyconnect-tooltip__text")[0];
	  var transitionend = __webpack_require__(14);
	
	
	  /**
	   * init
	   * @return {[type]} [description]
	   */
	  var init = function(){
	    //console.log(prefix('animation-end'));
	  };
	
	
	
	  /**
	   * animation
	   * @return {[type]} [description]
	   */
	  var animateIn = function(callback){
	
	    moduleElement.classList.add("animate-in");
	    //moduleElement.classList.remove("is-hidden");
	    moduleElement.addEventListener("transitionend", fireAfterTransitionAnimateIn, false);
	
	    //callback transition end
	    function fireAfterTransitionAnimateIn(){
	      //console.log("tooltip animate in called");
	
	      //if we have a special callback
	      if(callback !== undefined){
	        callback();
	      }
	      //or default behavior
	      else{
	        //console.log("fired ater transition");
	        //moduleElement.classList.add("is-visible");
	      }
	      // remove event listener
	      moduleElement.removeEventListener('transitionend', fireAfterTransitionAnimateIn, false);
	    }
	  };
	
	
	  var animateOut = function(callback){
	    //console.log("tooltip animate out called");
	
	    moduleElement.classList.remove("animate-in");
	    moduleElement.addEventListener("transitionend", fireAfterTransition, false);
	
	    //callback transition end
	    function fireAfterTransition(){
	
	
	      //if we have a special callback
	      if(callback !== undefined){
	        callback();
	      }
	      //or default behavior
	/*      else{
	        moduleElement.classList.add("is-hidden");
	      }*/
	      
	      // remove event listener
	      moduleElement.removeEventListener('transitionend', fireAfterTransition, false);
	    }
	  };
	
	
	
	  /**
	   * setMessage
	   * @return {[type]} [description]
	   */
	  var setMessage = function(message){
	    moduleElementText.textContent = message;
	  };
	
	
	  /**
	   * setStatus
	   * @return {[type]} [description]
	   */
	  var setStatus = function(status){
	    moduleElement.dataset.status = status;
	  };
	
	
	  return {
	    init: init,
	    element: function() { return moduleElement; },
	    animateIn: animateIn,
	    animateOut: animateOut,
	    setMessage: setMessage,
	    setStatus: setStatus
	  };
	
	})();

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	
	/* IZYCONNECT PROGRESSBAR */
	
	module.exports = (function(){
	  'use strict';
	
	  var moduleElement = document.getElementsByClassName("izyconnect-progressbar")[0];
	  var transitionend = __webpack_require__(14);
	
	
	
	  /**
	   * init
	   * @return {[type]} [description]
	   */
	/*
	  var init = function(){
	    _bindUiActions();
	  };
	*/
	
	
	
	
	  /**
	   * animation
	   * @return {[type]} [description]
	   */
	  var animateIn = function(callback){
	
	    moduleElement.classList.add("is-visible");
	    moduleElement.addEventListener("transitionend", fireAfterTransition, false);
	
	    //callback transition end
	    function fireAfterTransition(){
	
	      //if we have a special callback
	      if(callback !== undefined){
	        callback();
	      }
	
	      // remove event listener
	      moduleElement.removeEventListener('transitionend', fireAfterTransition, false);
	    }
	  };
	
	
	
	  /**
	   * hideElement
	   * @return {[type]} [description]
	   */
	  var hideElement = function(){
	    moduleElement.classList.remove("is-visible");
	  };
	
	
	  /**
	   * setProgress
	   * @return {[type]} [description]
	   */
	  var setProgress = function(step){
	    moduleElement.dataset.progress = step;
	  };
	
	
	  /**
	   * setProgress
	   * @return {[type]} [description]
	   */
	  var setSteps = function(step){
	    moduleElement.dataset.steps = step;
	  };
	
	
	
	  return {
	    animateIn: animateIn,
	    hideElement:hideElement,
	    setProgress:setProgress,
	    setSteps: setSteps,
	    element: function () { return moduleElement}
	  };
	
	})();

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	
	/* IZYCONNECT INPUT CODE */
	
	module.exports = (function(){
	  'use strict';
	
	
	  //dependencies
	  var VMasker = __webpack_require__(21);
	  var delayed = __webpack_require__(7);
	
	  //dom elements
	  var mElement = document.getElementsByClassName("izyconnect-input-code")[0];
	  var mElementInput = mElement.getElementsByClassName("izyconnect-input-code__input")[0];
	  var mElementSubmit = mElement.getElementsByClassName("izyconnect-input__submit")[0];
	
	  // components
	  var izyConnectLabel = __webpack_require__(15);
	  var izyConnectTooltip = __webpack_require__(18);
	  var izyConnectStatus = __webpack_require__(13);
	
	  // variables
	  //var keycodesNumbers = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 8, 46]; //numbers, backspace (8), delete (46)
	  var sendDataToApi;
	  var dataIsReadyToSubmit = false;
	  var userKey;
	  var userKeyPrefix;
	
	
	
	  /**
	   * init
	   * @return {[type]} [description]
	   */
	  var init = function(sendDataFunction, prefix){
	    //get callback function
	    sendDataToApi = sendDataFunction;
	
	    userKeyPrefix = prefix;
	    console.log("userKeyPrefix", userKeyPrefix);
	
	
	    //show input
	    mElement.classList.add("is-visible");
	    
	    //init mask on input
	    //VMasker(mElementInput).maskPattern("99999"); //okkey AA9999
	
	    //focus input
	    mElementInput.focus();
	    mElementInput.value = userKeyPrefix;
	
	    //watch input
	    mElementInput.addEventListener("keyup", _watchInput);
	
	     
	    //set label text
	    izyConnectLabel.setMessage("Code de validation");
	    izyConnectLabel.animateIn();
	
	    //set tooltip text
	    izyConnectTooltip.setStatus("info");
	    izyConnectTooltip.setMessage("Veuillez saisir le code reçu par sms ou utiliser l'application izyFill");
	    izyConnectTooltip.animateIn();
	
	  };
	
	  
	
	  /**
	   * _watchInput
	   * @return {[type]} [description]
	   */
	  var _watchInput = function(event) {
	
	    //allow only numbers
	    _filterInput();
	
	    //if key is return and gsm is valid
	    if(event.keyCode === 13 && dataIsReadyToSubmit){_weHaveValidData();}
	
	    //store unlock code
	    userKey = mElementInput.value.toString().replace(/[^0-9]/g, '');
	
	    //set progress
	    mElement.dataset.progress = userKey.length;
	
	
	    //console.log(userKey.length );
	    if (userKey.length === 4) {
	      _initBtnSubmit();
	    }
	    else{
	      _disableBtnSubmit();
	    }
	
	  }
	
	
	
	
	  /**
	   * _filterInput
	   * @return {[type]} [description]
	   */
	  var _filterInput = function() {
	    //var cleanKey = mElementInput.value.toString().replace(/[^0-9]/g, '');
	    mElementInput.value = userKeyPrefix + mElementInput.value.toString().replace(/[^0-9]/g, '');
	  }
	
	
	
	  /**
	   * _initBtnSubmit
	   * @return {[type]} [description]
	   */
	  var _initBtnSubmit = function(){
	    //set progress
	    mElement.dataset.progress = 5;
	
	    //console.log("destroy " + moduleElement);
	    mElementSubmit.addEventListener("click", _weHaveValidData, false);
	    mElementSubmit.addEventListener("touch", _weHaveValidData, false);
	
	    dataIsReadyToSubmit = true;
	  };
	
	
	  var _disableBtnSubmit = function(){
	    //console.log("destroy " + moduleElement);
	    mElementSubmit.removeEventListener("click", _weHaveValidData, false);
	    mElementSubmit.removeEventListener("touch", _weHaveValidData, false);
	
	    dataIsReadyToSubmit = false;
	  };
	
	
	  /**
	   * _weHaveValidData
	   * @return {[type]} [description]
	   */
	  var _weHaveValidData = function(){
	
	    //remove tooltip
	    izyConnectTooltip.animateOut();
	    izyConnectLabel.animateOut();
	
	    //set Status
	    izyConnectStatus.setMessage("Vérification du code");
	    izyConnectStatus.setStatus("loading");
	    izyConnectStatus.animateIn();
	
	    //sendDataToApi(gsmNumber);
	    console.log("userKey", userKey);
	    delayed.delay(function () {
	      sendDataToApi(userKey);
	    }, 2000)
	
	  };
	
	
	
	
	  /**
	   * hideInput
	   * @return {[type]} [description]
	   */
	  var hideInput = function(){
	    mElement.classList.remove("is-visible");
	  };
	
	  /**
	   * resetInput
	   * @return {[type]} [description]
	   */
	  var resetInput = function(){
	    mElementInput.value = "";
	  };
	
	  /**
	   * destroyInput
	   * @return {[type]} [description]
	   */
	  var destroyInput = function(){
	    //console.log("destroy " + moduleElement);
	    mElement.remove();
	  };
	
	
	
	
	
	
	
	  return {
	    init: init,
	    destroyInput:destroyInput,
	    hideInput: hideInput,
	    resetInput: resetInput,
	    element: function () { return mElement;}
	
	  };
	
	})();

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;(function(root, factory) {
	  if (true) {
	    !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  } else if (typeof exports === 'object') {
	    module.exports = factory();
	  } else {
	    root.VMasker = factory();
	  }
	}(this, function() {
	  var DIGIT = "9",
	      ALPHA = "A",
	      ALPHANUM = "S",
	      BY_PASS_KEYS = [8, 9, 16, 17, 18, 36, 37, 38, 39, 40, 91, 92, 93],
	      isAllowedKeyCode = function(keyCode) {
	        for (var i = 0, len = BY_PASS_KEYS.length; i < len; i++) {
	          if (keyCode == BY_PASS_KEYS[i]) {
	            return false;
	          }
	        }
	        return true;
	      },
	      mergeMoneyOptions = function(opts) {
	        opts = opts || {};
	        opts = {
	          precision: opts.hasOwnProperty("precision") ? opts.precision : 2,
	          separator: opts.separator || ",",
	          delimiter: opts.delimiter || ".",
	          unit: opts.unit && (opts.unit.replace(/[\s]/g,'') + " ") || "",
	          suffixUnit: opts.suffixUnit && (" " + opts.suffixUnit.replace(/[\s]/g,'')) || "",
	          zeroCents: opts.zeroCents,
	          lastOutput: opts.lastOutput
	        };
	        opts.moneyPrecision = opts.zeroCents ? 0 : opts.precision;
	        return opts;
	      },
	      // Fill wildcards past index in output with placeholder
	      addPlaceholdersToOutput = function(output, index, placeholder) {
	        for (; index < output.length; index++) {
	          if(output[index] === DIGIT || output[index] === ALPHA || output[index] === ALPHANUM) {
	            output[index] = placeholder;
	          }
	        }
	        return output;
	      }
	  ;
	
	  var VanillaMasker = function(elements) {
	    this.elements = elements;
	  };
	
	  VanillaMasker.prototype.unbindElementToMask = function() {
	    for (var i = 0, len = this.elements.length; i < len; i++) {
	      this.elements[i].lastOutput = "";
	      this.elements[i].onkeyup = false;
	      this.elements[i].onkeydown = false;
	
	      if (this.elements[i].value.length) {
	        this.elements[i].value = this.elements[i].value.replace(/\D/g, '');
	      }
	    }
	  };
	
	  VanillaMasker.prototype.bindElementToMask = function(maskFunction) {
	    var that = this,
	        onType = function(e) {
	          e = e || window.event;
	          var source = e.target || e.srcElement;
	
	          if (isAllowedKeyCode(e.keyCode)) {
	            setTimeout(function() {
	              that.opts.lastOutput = source.lastOutput;
	              source.value = VMasker[maskFunction](source.value, that.opts);
	              source.lastOutput = source.value;
	              if (source.setSelectionRange && that.opts.suffixUnit) {
	                source.setSelectionRange(source.value.length, (source.value.length - that.opts.suffixUnit.length));
	              }
	            }, 0);
	          }
	        }
	    ;
	    for (var i = 0, len = this.elements.length; i < len; i++) {
	      this.elements[i].lastOutput = "";
	      this.elements[i].onkeyup = onType;
	      if (this.elements[i].value.length) {
	        this.elements[i].value = VMasker[maskFunction](this.elements[i].value, this.opts);
	      }
	    }
	  };
	
	  VanillaMasker.prototype.maskMoney = function(opts) {
	    this.opts = mergeMoneyOptions(opts);
	    this.bindElementToMask("toMoney");
	  };
	
	  VanillaMasker.prototype.maskNumber = function() {
	    this.opts = {};
	    this.bindElementToMask("toNumber");
	  };
	  
	  VanillaMasker.prototype.maskAlphaNum = function() {
	    this.opts = {};
	    this.bindElementToMask("toAlphaNumeric");
	  };
	
	  VanillaMasker.prototype.maskPattern = function(pattern) {
	    this.opts = {pattern: pattern};
	    this.bindElementToMask("toPattern");
	  };
	
	  VanillaMasker.prototype.unMask = function() {
	    this.unbindElementToMask();
	  };
	
	  var VMasker = function(el) {
	    if (!el) {
	      throw new Error("VanillaMasker: There is no element to bind.");
	    }
	    var elements = ("length" in el) ? (el.length ? el : []) : [el];
	    return new VanillaMasker(elements);
	  };
	
	  VMasker.toMoney = function(value, opts) {
	    opts = mergeMoneyOptions(opts);
	    if (opts.zeroCents) {
	      opts.lastOutput = opts.lastOutput || "";
	      var zeroMatcher = ("("+ opts.separator +"[0]{0,"+ opts.precision +"})"),
	          zeroRegExp = new RegExp(zeroMatcher, "g"),
	          digitsLength = value.toString().replace(/[\D]/g, "").length || 0,
	          lastDigitLength = opts.lastOutput.toString().replace(/[\D]/g, "").length || 0
	      ;
	      value = value.toString().replace(zeroRegExp, "");
	      if (digitsLength < lastDigitLength) {
	        value = value.slice(0, value.length - 1);
	      }
	    }
	    var number = value.toString().replace(/[\D]/g, ""),
	        clearDelimiter = new RegExp("^(0|\\"+ opts.delimiter +")"),
	        clearSeparator = new RegExp("(\\"+ opts.separator +")$"),
	        money = number.substr(0, number.length - opts.moneyPrecision),
	        masked = money.substr(0, money.length % 3),
	        cents = new Array(opts.precision + 1).join("0")
	    ;
	    money = money.substr(money.length % 3, money.length);
	    for (var i = 0, len = money.length; i < len; i++) {
	      if (i % 3 === 0) {
	        masked += opts.delimiter;
	      }
	      masked += money[i];
	    }
	    masked = masked.replace(clearDelimiter, "");
	    masked = masked.length ? masked : "0";
	    if (!opts.zeroCents) {
	      var beginCents = number.length - opts.precision,
	          centsValue = number.substr(beginCents, opts.precision),
	          centsLength = centsValue.length,
	          centsSliced = (opts.precision > centsLength) ? opts.precision : centsLength
	      ;
	      cents = (cents + centsValue).slice(-centsSliced);
	    }
	    var output = opts.unit + masked + opts.separator + cents + opts.suffixUnit;
	    return output.replace(clearSeparator, "");
	  };
	
	  VMasker.toPattern = function(value, opts) {
	    var pattern = (typeof opts === 'object' ? opts.pattern : opts),
	        patternChars = pattern.replace(/\W/g, ''),
	        output = pattern.split(""),
	        values = value.toString().replace(/\W/g, ""),
	        charsValues = values.replace(/\W/g, ''),
	        index = 0,
	        i,
	        outputLength = output.length,
	        placeholder = (typeof opts === 'object' ? opts.placeholder : undefined)
	    ;
	    
	    for (i = 0; i < outputLength; i++) {
	      // Reached the end of input
	      if (index >= values.length) {
	        if (patternChars.length == charsValues.length) {
	          return output.join("");
	        }
	        else if ((placeholder !== undefined) && (patternChars.length > charsValues.length)) {
	          return addPlaceholdersToOutput(output, i, placeholder).join("");
	        }
	        else {
	          break;
	        }
	      }
	      // Remaining chars in input
	      else{
	        if ((output[i] === DIGIT && values[index].match(/[0-9]/)) ||
	            (output[i] === ALPHA && values[index].match(/[a-zA-Z]/)) ||
	            (output[i] === ALPHANUM && values[index].match(/[0-9a-zA-Z]/))) {
	          output[i] = values[index++];
	        } else if (output[i] === DIGIT || output[i] === ALPHA || output[i] === ALPHANUM) {
	          if(placeholder !== undefined){
	            return addPlaceholdersToOutput(output, i, placeholder).join("");
	          }
	          else{
	            return output.slice(0, i).join("");
	          }
	        }
	      }
	    }
	    return output.join("").substr(0, i);
	  };
	
	  VMasker.toNumber = function(value) {
	    return value.toString().replace(/(?!^-)[^0-9]/g, "");
	  };
	  
	  VMasker.toAlphaNumeric = function(value) {
	    return value.toString().replace(/[^a-z0-9 ]+/i, "");
	  };
	
	  return VMasker;
	}));


/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	
	/* IZYCONNECT INPUT SUBSCRIBE */
	
	module.exports = (function(){
	  'use strict';
	
	  //libs
	  var email = __webpack_require__(23);
	  var delayed = __webpack_require__(7);
	
	  // dom elements
	  var mElement = document.getElementsByClassName("izyconnect-input-subscribe")[0];
	  var mElementInputNom = mElement.getElementsByClassName("izyconnect-input-subscribe__input -nom")[0];
	  var mElementInputPrenom = mElement.getElementsByClassName("izyconnect-input-subscribe__input -prenom")[0];
	  var mElementInputEmail = mElement.getElementsByClassName("izyconnect-input-subscribe__input -email")[0];
	  var mElementSubmit = mElement.getElementsByClassName("izyconnect-input__submit")[0];
	
	
	  // modules
	  var izyConnectLabel = __webpack_require__(15);
	  var izyConnectTooltip = __webpack_require__(18);
	  var izyConnectProgressbar = __webpack_require__(19);
	  var izyConnectStatus= __webpack_require__(13);
	
	  // variables
	  var sendDataToApi;
	  var userData = {};
	  var subscriptionStep = 1;
	  var activeInput;
	
	
	
	
	  /**
	   * init
	   * @return {[type]} [description]
	   */
	  var init = function(sendDataFunction){
	
	    //show input
	    mElement.classList.add("is-visible");
	
	    //store callback function
	    sendDataToApi = sendDataFunction;
	
	    //swich nav mode on label
	    izyConnectLabel.enableNav();
	    izyConnectLabel.animateIn();
	    izyConnectLabel.setNavStep(subscriptionStep);
	
	    //bind click on nav elements
	    izyConnectLabel.elementNav().addEventListener("click", _handleClickNav, true);
	
	    // init form for current step
	    _initSubscribeStep();
	
	    //show tooltip
	    izyConnectTooltip.setMessage("Pour créer votre compte nous avons besoin de quelques informations…");
	    izyConnectTooltip.setStatus("info");
	    izyConnectTooltip.animateIn();
	
	  };
	
	
	
	
	
	  /**
	   * _initSubscribeStep
	   * @return {[type]} [description]
	   */
	  var _initSubscribeStep = function(){
	
	    _setStep();
	    _cacheActiveInput();
	    _validateActiveInput();
	
	    switch (subscriptionStep) {
	      case 1: //init email
	
	        //show label
	        izyConnectLabel.setNavStep(1);
	
	        //set progressbar
	        izyConnectProgressbar.setProgress("0/3/3");
	
	        //watch input
	        mElementInputEmail.addEventListener("keyup", _validateActiveInput);
	        mElementInputEmail.focus();
	
	        break;
	
	      case 2: //init prenom
	
	        //show label
	        izyConnectLabel.setNavStep(2);
	
	        //set progressbar
	        izyConnectProgressbar.setProgress("1/3/3");
	
	        //watch input
	        mElementInputPrenom.addEventListener("keyup", _validateActiveInput);
	        mElementInputPrenom.focus();
	        
	        break;
	
	
	      case 3: //init nom
	
	        //show label
	        izyConnectLabel.setNavStep(3);
	
	        //set progressbar
	        izyConnectProgressbar.setProgress("2/3/3");
	
	        //watch input
	        mElementInputNom.addEventListener("keyup", _validateActiveInput);
	        mElementInputNom.focus();
	
	      default:
	        console.log("default step");
	    }
	
	  };
	
	
	
	
	
	  /**
	   * _validateActiveInput
	   * @return {[type]} [description]
	   */
	  var _validateActiveInput = function(event) {
	
	    //filter characters
	    _filterInput();
	
	    //define last key pressed
	    var lastKeyPressed = typeof event === 'undefined' ? false : event.keyCode || event.which;
	
	
	    /* check active input */
	    if(activeInput === mElementInputEmail){
	      console.log("validation input email");
	      validateInputEmail();
	    }
	    else{
	      console.log("validation input texte");
	      validateInputText();
	    }
	
	
	
	
	    /*
	      validate input email
	    */
	    function validateInputEmail(event){
	
	      if (email.isValid(mElementInputEmail.value)){
	        _enableBtnSubmit();
	
	        //if key is not a number, stop here
	        if(lastKeyPressed && lastKeyPressed === 13){
	          _handleClick();
	        }
	      }
	      else{
	        _disableBtnSubmit();
	
	        if(mElementInputEmail.value.length > 4){
	          console.log("add error message");
	        }
	      }
	     };
	
	
	     /*
	      validate input text
	     */
	     function validateInputText () {
	       if (activeInput.value.length > 2){
	         _enableBtnSubmit();
	
	         //if key is not a number, stop here
	         if(lastKeyPressed && lastKeyPressed === 13){
	           _handleClick();
	         }
	       }
	     }
	
	  };
	
	
	
	
	  /**
	   * _filterInput
	   * @return {[type]} [description]
	   */
	  var _filterInput = function() {
	    if(activeInput === mElementInputEmail){ //validation email
	      var filterCaracters = "/[^a-zA-Z0-9\u00C0-\u00FF\@\+\_\-\.]/g";
	    }
	    else{
	      var filterCaracters = "/[^a-zA-Z0-9\u00C0-\u00FF\ \_\-\.]/g";
	    }
	
	    activeInput.value = activeInput.value.toString().replace(filterCaracters, '');
	  }
	
	
	
	  /**
	   * _watchInputPrenom
	   * @return {[type]} [description]
	   */
	/*
	   var _watchInputPrenom = function(element) {
	      console.log("keydown");
	      console.log("mElementInputPrenom.value.length", mElementInputPrenom.value.length);
	      if (mElementInputPrenom.value.length > 3){
	        _enableBtnSubmit();
	
	        //if key is not a number, stop here
	        if(element.keyCode === 13){
	          _handleClick();
	        }
	      }
	   };
	*/
	
	  /**
	   * _watchInputNom
	   * @return {[type]} [description]
	   */
	/*  var _watchInputNom = function(element) {
	    console.log("keydown");
	    if (mElementInputNom.value.length > 3){
	      _enableBtnSubmit();
	
	      //if key is not a number, stop here
	      if(element.keyCode === 13){
	        _handleClick();
	      }
	    }
	  };*/
	
	
	
	  /**
	   * _watchInputEmail
	   * @return {[type]} [description]
	   */
	/*  var _watchInputEmail = function(element) {
	    if (email.isValid(mElementInputEmail.value)){
	      _enableBtnSubmit();
	
	      //if key is not a number, stop here
	      if(element.keyCode === 13){
	        _handleClick();
	      }
	    }
	    else{
	      _disableBtnSubmit();
	
	      if(mElementInputEmail.value.length > 4){
	        console.log("add error message");
	      }
	    }
	  };*/
	
	
	
	
	  /**
	   * _enableBtnSubmit
	   * @return {[type]} [description]
	   */
	  var _enableBtnSubmit = function(element) {
	    mElementSubmit.addEventListener("click", _handleClick, false);
	    mElementSubmit.addEventListener("touch", _handleClick, false);
	
	    mElement.dataset.showsubmit = "true";
	  };
	
	
	  /**
	   * _disableBtnSubmit
	   * @return {[type]} [description]
	   */
	  var _disableBtnSubmit = function(element) {
	    mElementSubmit.removeEventListener("click", _handleClick, false);
	    mElementSubmit.removeEventListener("touch", _handleClick, false);
	
	    mElement.dataset.showsubmit = "false";
	  };
	
	
	
	
	
	  /**
	   * _handleClick
	   * @return {[type]} [description]
	   */
	  var _handleClick = function(){
	    //hide btn submit
	    _disableBtnSubmit();
	
	
	    switch (subscriptionStep) {
	
	      case 1: //subscription email
	        //if nom valid
	        userData.email = mElementInputEmail.value;
	
	        //removeEventtListener input prenom
	        mElementInputEmail.removeEventListener("keyup", _validateActiveInput);
	
	        //set progressbar
	        subscriptionStep++;
	        _initSubscribeStep();
	
	        break;
	
	      case 2: //subscription prenom
	        //if prenom valid
	        userData.prenom = mElementInputPrenom.value;
	
	        //removeEventtListener input prenom
	        mElementInputPrenom.removeEventListener("keyup", _validateActiveInput);
	
	        //set progressbar
	        subscriptionStep++;
	        _initSubscribeStep();
	
	        break;
	
	      case 3: //subscription nom
	
	        //if nom valid
	        userData.nom = mElementInputNom.value;
	
	        //removeEventtListener input prenom
	        mElementInputNom.removeEventListener("keyup", _validateActiveInput);
	
	        //send data
	        _weHaveValidData();
	
	        break;
	
	
	      default:
	        console.log("default response");
	    }
	
	  };
	
	
	
	
	  /**
	   * _handleClickNav
	   * @return {[type]} [description]
	   */
	  var _handleClickNav = function(event){
	
	    // check if it's a span
	    if (!event.target.classList.contains("izyconnect-label__nav__item")){return;}
	
	    // check if it's a previous step
	    var targetStep = parseInt(event.target.dataset.target);
	    if(targetStep >= subscriptionStep ){return;}
	
	
	    console.log("switching to step" + targetStep);
	    subscriptionStep = targetStep;
	    _initSubscribeStep(targetStep);
	  };
	
	
	
	  /**
	   * setStep
	   * @return {[type]} [description]
	   */
	  var _setStep = function(){
	    mElement.dataset.step = subscriptionStep;
	  };
	
	
	
	  /**
	   * _cacheActiveInput
	   * @return {[type]} [description]
	   */
	  var _cacheActiveInput = function(){
	    console.log("caching active input");
	    console.log("subscriptionStep", subscriptionStep);
	
	    switch (subscriptionStep) {
	      case 1:
	        activeInput = mElementInputEmail;
	        break;
	      case 2:
	        activeInput = mElementInputPrenom;
	        break;
	      case 3:
	        activeInput = mElementInputNom;
	        break;
	      default:
	        throw new Error("Can't define active input");
	    }
	  };
	
	
	
	
	
	
	  /**
	   * _weHaveValidData
	   * @return {[type]} [description]
	   */
	  var _weHaveValidData = function(){
	    console.log(userData);
	
	    //set progressbar
	    izyConnectProgressbar.setProgress("3/3/3");
	
	    //remove tooltip
	    izyConnectTooltip.animateOut();
	    izyConnectLabel.animateOut();
	    izyConnectLabel.disableNav();
	
	    //set Status
	    izyConnectStatus.setMessage("Vérification des données");
	    izyConnectStatus.setStatus("loading");
	    izyConnectStatus.animateIn();
	
	
	    //sendDataToApi();
	    delayed.delay(function () {
	      sendDataToApi(userData);
	    }, 1000)
	
	  };
	
	
	
	
	  /**
	   * hideInput
	   * @return {[type]} [description]
	   */
	  var hideInput = function(){
	    mElement.classList.remove("is-visible");
	  };
	
	  /**
	   * resetInput
	   * @return {[type]} [description]
	   */
	  var resetInput = function(){
	    mElementInputNom.value = "";
	    mElementInputPrenom.value = "";
	    mElementInputEmail.value = "";
	  };
	
	  /**
	   * destroyInput
	   * @return {[type]} [description]
	   */
	  var destroyInput = function(){
	    mElement.remove();
	  };
	
	
	
	
	
	
	
	  return {
	    init: init,
	    destroyInput: destroyInput,
	    hideInput: hideInput,
	    resetInput: resetInput,
	    element: function () { return mElement;},
	    elementInputNom: function () { return mElementInputNom;},
	    elementInputPrenom: function () { return mElementInputPrenom;},
	    elementInputEmail: function () { return mElementInputEmail;}
	
	  };
	
	})();

/***/ },
/* 23 */
/***/ function(module, exports) {

	// NOTE: Due to the schema implementation using regex flags like i, g or s is not supported
	
	// Email validation that does handle CJK, Cyrillic, Devangari, Hiragana,
	// Arabic and require at least 2 letters in top-level domain.
	var validChars = function(extra, quantifier) {
		var accents = 'ÁáÀàÂâÆæÄäÇçÉéÈèÊêËëÍíÎîÏïÑñÓóÔôŒœÖöÚúÙùÛûÜüŸÿ';
		var unicodes = '\\u4e00-\\u9eff\\u0400-\\u04ff\\u0900-\\u097f\\u3040-\\u309f\\u0600-\\u06ff';
		extra = typeof extra === 'string' ? extra : '';
		quantifier = (['*', '+', '?'].indexOf(quantifier) > -1 ? quantifier : '');
	
		return [
			'[','A-Za-z0-9', accents, unicodes, extra,']',
			quantifier
		].join('');
	};
	
	var local = validChars("!#$%&'*+/=?^_`{|}~-", '+')+"(?:\\."+validChars("!#$%&'*+/=?^_`{|}~-", '+')+")*";
	var domain = "(?:"+validChars()+"(?:"+validChars('-', '*')+validChars()+")?\\.)+";
	var topLevelDomain = validChars()+"(?:"+validChars('-', '*')+validChars()+")";
	
	var emailPattern = new RegExp('^' + local + '@' + domain + topLevelDomain + '$', 'i');
	
	var emailPatternSource = emailPattern.source.slice(1, -1);
	var multipleEmailPattern = new RegExp('^\\s*(?:' + emailPatternSource + '\\s*,\\s*)*' + emailPatternSource + '$');
	
	var emailPatternOrEmptyString = new RegExp('^$|' + emailPattern.source, 'i');
	
	function isValid(email) {
		return emailPattern.test(email);
	}
	
	function isMultipleValid(emails) {
		if (typeof emails !== 'string') {
			return false;
		}
		return emails.trim().split(/\s*,\s*/).every(isValid);
	}
	
	module.exports = {
		single: emailPattern,
		multiple: multipleEmailPattern,
		optional: emailPatternOrEmptyString,
		isValid: isValid,
		isMultipleValid: isMultipleValid
	};


/***/ }
/******/ ]);
//# sourceMappingURL=app.js.map